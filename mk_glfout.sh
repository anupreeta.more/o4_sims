## Move log files and delete unwanted files 
#  mv glflog/?.$2"err" glflog/*.$2"out" $1"/glaf_log_"$2"/"
#  rm  glflog/*.$2"log" $1/"wc_"$2"_"*".txt"  glflog/*.$2"err"
#
## Combine final output catalogs
# cat $1/out_fincat_$2/imgprop_2_*txt > $1/out_fincat_$2/dblall_imgprop.txt
# cat $1/out_fincat_$2/imgprop_4_*txt > $1/out_fincat_$2/quadall_imgprop.txt
# cat $1/out_fincat_$2/lensprop_*txt  > $1/out_fincat_$2/all_lensprop.txt
 
## Sanity check for number of rows written in output catalogs. They should match.
  ab=`wc -l $1/out_fincat_$2/dblall_imgprop.txt | awk '{print $1}'` ; f1=`expr $ab / 2`
  cd=`wc -l $1/out_fincat_$2/quadall_imgprop.txt | awk '{print $1}'` ; f2=`expr $cd / 4`
  echo "Total number of double and quad systems: " `expr $f1 + $f2`
  wc -l $1/out_fincat_$2/all_lensprop.txt | awk '{print "Total number of lenses:",$1}'

