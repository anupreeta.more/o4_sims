import numpy as np
from scipy.integrate import quad,fixed_quad
from scipy.interpolate import UnivariateSpline
from subprocess import call
import pandas
import lensprop as lp
import writefile_O4 as wf
import srclensprop as slp
import os
from constants import *
import sys
from main import *

## Input label and processor number
lab = sensitivity  
rank = nproc 


np.random.seed(gseed+91000+rank)

fdir = "%s_%s_%s"%(lab, detlabel, srctype)

df = pandas.read_csv("selcat_%s_%s_%s.txt"%(lab, detlabel, srctype),sep=" ",header=None, names=(["selid", "zlens", "gal_vd", "gal_ell","gal_pa","zsrc","uSNR","tau","lensflag","numlensevt_boost","boostflag"]))

niter = 100
batch= rank*200
batsize=batch+200

## Read catalog with lens properties and source redshift, SNR
selid, zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR, tau, lensflag, numlensevt_boost, boostflag = df.selid.values[batch:batsize], df.zlens.values[batch:batsize], df.gal_vd.values[batch:batsize], df.gal_ell.values[batch:batsize], df.gal_pa.values[batch:batsize], df.zsrc.values[batch:batsize], df.uSNR.values[batch:batsize], df.tau.values[batch:batsize], df.lensflag.values[batch:batsize], df.numlensevt_boost.values[batch:batsize], df.boostflag.values[batch:batsize]
 

call("rm %s/out_fincat_%s/imgprop_2_%04d_cat.txt"%(fdir, lab, rank), shell=1)
call("rm %s/out_fincat_%s/imgprop_4_%04d_cat.txt"%(fdir, lab, rank), shell=1)
fout2 = open("%s/out_fincat_%s/imgprop_2_%04d_cat.txt"%(fdir, lab, rank), "a")
fout4 = open("%s/out_fincat_%s/imgprop_4_%04d_cat.txt"%(fdir, lab, rank), "a")
fp1=open("%s/out_fincat_%s/lensprop_%04d_cat.txt"%(fdir, lab, rank),'w')

for ii in range(zlens.size):
    fname="%s/glfiles_%s/glinp_%d"%(fdir, lab, batch+ii)
    outname="%s/glout_%s/outgal_%d"%(fdir, lab, batch+ii)
    srcrow="%s/output_%s/srcrow_%d.txt"%(fdir, lab, batch+ii)
    imgout="%s/output_%s/imgout_%d.txt"%(fdir, lab, batch+ii)

    ## Determine the window size and grid pixel size over which the lens equations will be solved        
    ## Need to adapt it according to the Einstein radius of a given lens mass model
    rein=slp.getreinst(zlens[ii],zsrc[ii],gal_vd[ii]) 
    rein_arcsec=rein*206265
    winhalfsize= np.nanmax([rein_arcsec*5.,1]) 
    pix_ext=np.float32("%.5f"%(winhalfsize*0.168/5.))
    pix_poi=np.float32("%.5f"%(rein_arcsec/35.))
    
    ## Create input file for Glafic
    wf.write_inpfile(fname,outname,zlens[ii], gal_vd[ii], gal_ell[ii], gal_pa[ii], zsrc[ii],winhalfsize,pix_poi)
    print("%s %s %d %.3f %.2f %.2f %.2f %.3f %.2f %e %d %.2f %.5f"%(fname,outname,selid[ii],zlens[ii], gal_vd[ii], gal_ell[ii], gal_pa[ii], zsrc[ii],uSNR[ii],numlensevt_boost[ii],boostflag[ii],rein_arcsec, pix_poi))

    ## Run glafic to generate O(1000) sources behind a lens and extract the lensed image properties
    call("glafic %s > /dev/null 2>&1 "%(fname),shell=1)
    call("./getsrcprop_O4_1.sh %.4f %s_mock.dat %s %s/wc_%s_%s.txt"%(zsrc[ii],outname,srcrow,fdir,lab,rank),shell=1)
    print("./getsrcprop_O4_1.sh %.4f %s_mock.dat %s %s/wc_%s_%s.txt"%(zsrc[ii],outname,srcrow,fdir,lab,rank))
    
    ## Draw niter sources from among thousands
    rowmax=np.loadtxt("%s/wc_%s_%s.txt"%(fdir,lab,rank))
    rowtosel=np.arange(rowmax)
    np.random.shuffle(rowtosel)

    ## Loop over each source
    for jj in range(niter):
        if(os.path.exists(imgout)):
           call("rm %s"%(imgout),shell=1)
        try:
           rownum=rowtosel[jj]
        except IndexError:
           print("IndexError at", jj,"for lens", batch+ii)
           continue
        ## Extract the source at a given row number in the output file and store image properties in a file
        call("./getsrcprop_O4_2.sh %d %s %s_mock.dat %s "%(rownum,srcrow,outname,imgout),shell=1)
        print("./getsrcprop_O4_2.sh %d %s %s_mock.dat %s "%(rownum,srcrow,outname,imgout))

        ## Open the file with the image properties
        try:
            imx,imy,immag,imtdel=np.loadtxt(imgout,unpack=1)
            mag_im2= sorted(np.abs(immag),reverse=True)[1]
            ## check if lensed SNR above SNR threshold for the 2nd brightest image
            if(uSNR[ii]*np.sqrt(mag_im2) > SNRthresh):
                indx=np.argsort(imtdel)
                imtdels=imtdel[indx]
                immags=immag[indx]
                imxs=imx[indx]
                imys=imy[indx]
                imnum=imxs.size
                ID=(batch+ii)*np.ones(imnum,dtype=int)
                parity=immags/np.abs(immags)
                ## Store doubly lensed and quadruply lensed images in separate files
                if(imnum==2):
                    wf.write_pandascat_1(fout2, ID, jj,imxs,imys,immags,imtdels,parity)
                elif(imnum==4):
                    if(immags[1]<immags[2] and np.abs(imtdels[1]-imtdels[2])<=0.5):
                       indx=[0,2,1,3]
                    else:
                       indx=[0,1,2,3]
                    wf.write_pandascat_1(fout4, ID[indx], jj, imxs[indx],imys[indx],immags[indx],imtdels[indx], parity[indx])
                ## Write lens properties
                fp1.write("%d %d %5.3f %7.2f %5.3f %7.2f %5.2f %5.3f %d %d %e %d\n"%(ID[0], jj, zlens[ii], gal_vd[ii], gal_ell[ii], gal_pa[ii], zsrc[ii], uSNR[ii],imnum, selid[ii], numlensevt_boost[ii]/tau[ii], boostflag[ii]))
                print("## Writing ",imgout,ID[0],jj,zlens[ii], gal_vd[ii], gal_ell[ii], gal_pa[ii], zsrc[ii], uSNR[ii],imnum, lensflag[ii], numlensevt_boost[ii], boostflag[ii])
                sys.stdout.flush
            else:
                print("## ",batch+ii,"lensed SNR less than the threshold", uSNR[ii], uSNR[ii]*np.sqrt(mag_im2), lensflag[ii], numlensevt_boost[ii], boostflag[ii])
        except ValueError:
            print("## %s is Empty"%(imgout))
            continue

fout2.close()
fout4.close()
fp1.close()

