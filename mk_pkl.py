import numpy as np
import sys
from scipy.integrate import fixed_quad,quad
from scipy.interpolate import interp1d 
from scipy.interpolate import UnivariateSpline
import pickle
from subprocess import call
import os
from constants import *

def SNRsplines(sensitivity,detectors,srctype=None):

    onepluszs= np.logspace(np.log10(1+sn_zsmin),np.log10(1+sn_zsmax),sn_zbins)
    zs=onepluszs-1

    max_snr = np.zeros(sn_zbins)
    histbins = 100
    histo = np.zeros(histbins*sn_zbins).reshape(sn_zbins, histbins)
    d = {}
    d["zs"] = zs
    d["splines"] = {}
                 
    for ii in range(sn_zbins):
        print(ii)
        snr=np.loadtxt("%s_%s_%s/netwsnr_%s/netsnr_%02d.txt"%(sensitivity,detectors,srctype,sensitivity,ii))
        arr = np.sort(snr)[:int(0.999*snr.size)]
        max_snr[ii] = arr[-1]
        renorm_arr = arr/max_snr[ii]

        # Get a histogram of the renorm_arr and store it 
        nn, bins = np.histogram(renorm_arr, bins=np.linspace(0., 1.0, histbins+1))
        hist = np.cumsum(nn)/np.sum(nn)
        hist = np.insert(hist, 0, 0)
       
        d["splines"]["%.3f" % (zs[ii])] = interp1d(hist, bins)

    d["maxsnr_spl"] = interp1d(zs, max_snr)
    if os.path.exists("pickled_files")==False: 
        call("mkdir pickled_files",shell=True)
    if srctype == None:
        srctype="BBH"   
        print("srctype is None. Using default srctype: BBH")

    print("Setting : ", sensitivity, detectors, srctype)
    fout = open("pickled_files/snr%s_%s_%s.pkl" %(sensitivity,detectors,srctype), "wb")
    pickle.dump(d, fout)
    fout.close()

if __name__ == "__main__":
    sensitivity=sys.argv[1]
    detectors=sys.argv[2]
    srctype=sys.argv[3]
    SNRsplines(sensitivity,detectors,srctype)
