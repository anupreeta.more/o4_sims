import numpy as np
from scipy.integrate import quad,fixed_quad
from scipy.interpolate import UnivariateSpline
from subprocess import call
import pandas
import srclensprop as slp
import os
from constants import *
import sys
from main import *

## Input label and processor number
lab = sensitivity  

## seed for iteration 2 
np.random.seed(gseed+18000)
fdir = "%s_%s_%s"%(lab, detlabel, srctype)

## No of sources to be chosen for a given lens for sufficient statistics; Note there is always 1 lens + 1 source. For simplicity, the lens is kept to be the same for N sources
df = pandas.read_csv("catalog_ell_%s_%s_%s.txt"%(lab, detlabel, srctype),sep=" ",header=None, names=(["zlens", "gal_vd", "gal_ell","gal_pa","zsrc","uSNR"]))


## Read catalog with lens properties and source redshift, SNR
zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR=df.zlens.values, df.gal_vd.values, df.gal_ell.values, df.gal_pa.values, df.zsrc.values, df.uSNR.values

## Determine the number of lensed events given the lensing cross-section
tau=slp.optical_depth(zlens, gal_vd, 1.-gal_ell, zsrc)

print(np.min(tau), np.max(tau), np.mean(tau), np.median(tau) )

