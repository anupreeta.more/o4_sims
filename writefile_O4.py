import numpy as np
import pandas

def write_inpfile_sh(fname,outname,zlens, gal_vd, gal_ell, gal_pa, shear, sh_pa, zsrc,halfsize,pix_poi):
   fp=open(fname,"w")
   fp.write("omega     0.30 \nlambda    0.70 \nweos      -1.000 \nhubble    0.700\n")
   fp.write("zl       %.2f \nprefix    %s \n"%(zlens,outname)) 
   fp.write("xmin    -%d \nymin    -%d \nxmax    %d \nymax    %d \n"%(halfsize, halfsize, halfsize, halfsize))
  #fp.write("pix_ext   0.1680 \npix_poi   1.000 \nmaxlev  2 \nflag_extnorm = 1 \n")
   fp.write("pix_ext   0.1680 \npix_poi   %.5f \nmaxlev  4 \nflag_extnorm = 1 \n"%(pix_poi))
   fp.write("\nstartup 2 0 0 \n")
   fp.write("lens sie  %.2f 0. 0. %.2f %.2f 0.0 0.0 \n"%(gal_vd,gal_ell, gal_pa))
   fp.write("lens pert %.2f 0. 0. %.2f %.2f 0.0 0.0 \n"%(zsrc,shear, sh_pa))
   fp.write("end_startup \nstart_command \nmock3 2000 %f 1.0 \nquit"%(zsrc)) ##\nwritecrit 2.0 
   fp.close()

def write_inpfile(fname,outname,zlens, gal_vd, gal_ell, gal_pa, zsrc,halfsize,pix_poi):
   fp=open(fname,"w")
   fp.write("omega     0.30 \nlambda    0.70 \nweos      -1.000 \nhubble    0.700\n")
   fp.write("zl       %.2f \nprefix    %s \n"%(zlens,outname)) 
   fp.write("xmin    -%d \nymin    -%d \nxmax    %d \nymax    %d \n"%(halfsize, halfsize, halfsize, halfsize))
  #fp.write("pix_ext   0.1680 \npix_poi   1.000 \nmaxlev  2 \nflag_extnorm = 1 \n")
   fp.write("pix_ext   0.1680 \npix_poi   %.5f \nmaxlev  4 \nflag_extnorm = 1 \n"%(pix_poi))
   fp.write("\nstartup 1 0 0 \n")
   fp.write("lens sie  %.2f 0. 0. %.2f %.2f 0.0 0.0 \n"%(gal_vd,gal_ell, gal_pa))
  #fp.write("end_startup \nstart_command \nmock3 20 %f 1.0 \nquit"%(zsrc)) ##\nwritecrit 2.0 
   fp.write("end_startup \nstart_command \nmock3 2000 %f 1.0 \nquit"%(zsrc)) ##\nwritecrit 2.0 
   fp.close()

def write_inpfile_nondet(fname,outname,zlens, gal_vd, gal_ell, gal_pa, zsrc,halfsize,pix_poi):
   fp=open(fname,"w")
   fp.write("omega     0.30 \nlambda    0.70 \nweos      -1.000 \nhubble    0.700\n")
   fp.write("zl       %.2f \nprefix    %s \n"%(zlens,outname)) 
   fp.write("xmin    -%d \nymin    -%d \nxmax    %d \nymax    %d \n"%(halfsize, halfsize, halfsize, halfsize))
   fp.write("pix_ext   0.1680 \npix_poi   %.5f \nmaxlev  4 \nflag_extnorm = 1 \n"%(pix_poi))
   fp.write("\nstartup 1 0 0 \n")
   fp.write("lens sie  %.2f 0. 0. %.2f %.2f 0.0 0.0 \n"%(gal_vd,gal_ell, gal_pa))
   fp.write("end_startup \nstart_command \nmock3 1000 %f 1.0 \nquit"%(zsrc)) ##\nwritecrit 2.0 
   fp.close()


##def write_pandascat(fout,ID,imxs,imys,immags,imtdels, imparity, header=False):
def write_pandascat_1(fout,ID,jj,imxs,imys,immags,imtdels, imparity, header=False):
    d = {}
    d["ID"] = ID
    d["imx"] = imxs
    d["imy"] = imys
    d["immag"] = immags
    d["imtdel"] = imtdels
    d["parity"] = imparity
    d["ID1"] = jj
    ## Write first iteration directly
    pandas.DataFrame(data=d).to_csv(fout, index=False, header=header, sep=" ")
  
