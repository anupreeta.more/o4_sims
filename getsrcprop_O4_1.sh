#!/bin/bash

awk -v var=$1 -f get_src_rownum.awk $2  > $3
wc -l $3 | awk '{print $1}'  > $4
echo "## " $1 $2 $3 $4
