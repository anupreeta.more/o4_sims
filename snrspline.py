import numpy as np
from pylab import *
from scipy.interpolate import interp2d #, interp1d
from scipy.interpolate import UnivariateSpline
import pickle

fdir = "O3_LHV_BBH_i2pi"

def SNRsplines():
    # Set the min max to those used in generating netsnr files
    zsmin=0.001
    zsmax=16.
    ## The number of zbins is matched with number of netsnr files
    zbins=50
    hist_bins=30
    onepluszs= np.logspace(np.log10(1+zsmin),np.log10(1+zsmax),zbins)
    zs=onepluszs-1
    max_snr = np.zeros(zbins)
    prob_snr = np.zeros(zbins*hist_bins).reshape(zbins,hist_bins)

    for ii in range(zbins):
        ## O3, network snr (L1,H1,V1)
        snr=np.loadtxt("%s/netwsnr_O3/netsnr_%02d.txt"%(fdir, ii))
        arr = np.sort(snr)[:int(0.999*snr.size)]
        max_snr[ii] = arr[-1]
        renorm_arr = arr/max_snr[ii]
        nn, bins = np.histogram(renorm_arr, bins=np.linspace(0, 1, hist_bins+1), density=True)
        bins=bins[1:]/2. + bins[:-1]/2.
        prob_snr[ii,:]=np.log10(nn)

   #snr_spl=interp2d(bins,zs,prob_snr)
    snr_spl=interp2d(bins[1:],zs,prob_snr[:, 1:])
    z_snr_spl=UnivariateSpline(zs,max_snr,s=0)

    return z_snr_spl, snr_spl


z_snr_spl, snr_spl = SNRsplines()

def probofsnr(snr,zsrc):
   
    max_snr_val=z_snr_spl(zsrc)
    probofsnr= 1./(max_snr_val) * 10**snr_spl(snr/max_snr_val,zsrc)
  
    return  probofsnr



plot_psnr= True 
if (plot_psnr==True):

    ## tested and gives correct normalised probability
    def test_probofsnr(zs):
        snr_max=z_snr_spl(zs)
        psnr=np.zeros(100)
        snr_arr=np.zeros(100)
        for ii in range(100):
            snr_arr[ii]=ii*snr_max/100
            psnr[ii]=probofsnr(ii*snr_max/100.,zs)
        return snr_arr,psnr

    ##############
    zs=np.array(["0.1905","1.883","0.336","0.8904"])
   #zs=np.array(["0.3","2.0","0.45","1.0"])
    fno=np.array(["03","18","05","11"])
    xrng=np.array([70, 6, 40, 10])
    yrng=np.array([0.13, 1.0, 0.25, 0.5])
    for ii in range (4): 
        ax=subplot(2,2,ii+1)
        zsf=np.float(zs[ii])        

 
        ## anu - analytical
        snr_arr,psnr=test_probofsnr(zsf)
        snr=np.loadtxt("%s/netwsnr_O3/netsnr_%s.txt"%(fdir, fno[ii]))
        ax.hist(snr,bins=50,density=True,label="Anu")
        ax.plot(snr_arr,psnr,label="From spline")

        ax.text(0.8,0.8,"z=%s"%(zsf),transform=ax.transAxes)
        ax.set_xlim(0,xrng[ii])
        ax.set_ylim(0,yrng[ii])
        ax.set_xlabel("SNR")
        ax.legend()
        savefig("/home/anupreeta.more/public_html/snrtest_mod_zmin0p001_i2pi_comp.pdf")

