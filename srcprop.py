import gwpopulation
from scipy.interpolate import interp1d
import numpy as np
from scipy.stats import norm 
import numpy as np
import sys
from scipy.integrate import quad
from scipy.interpolate import UnivariateSpline
import pickle
from constants import *

class srcMassModel:
    def __init__(self, massmod):
        self.massmod = massmod


    def sample_m1_m2(self, params, Nsamples): 
     
        Ninterp=20000    
        mmin = params["mmin"] 
        mmax = params["mmax"] 
        delta_m = params["delta_m"]
        dataset =  {}
        dataset["mass_1"] = np.linspace(mmin, mmax, Ninterp)

        if self.massmod=="SinglePeakSmoothedMassDistribution":
            lam = params["lam"]
            alpha = params["alpha"]
            beta = params["beta"]
            mpp = params["mpp"]
            sigpp = params["sigpp"]

            mass_model = gwpopulation.models.mass.SinglePeakSmoothedMassDistribution(mmin=mmin, mmax=mmax)

            prob = mass_model.p_m1(dataset, mmin=mmin, mmax=mmax, delta_m=delta_m, lam=lam, alpha=alpha, mpp=mpp, sigpp=sigpp)

        elif self.massmod=="DoublePeakSmoothedMassDistribution":
            
            print("tbd")
            ## mass_model , prob


        sample_m1 = interp1d(np.cumsum(prob)/np.sum(prob), dataset["mass_1"])
        
        m1 = sample_m1(np.random.random(size=Nsamples))
        
        # First initialize spline for m2
        ddataset =  {}
        ddataset["mass_1"] = np.zeros(Ninterp)+mmax
        ddataset["mass_ratio"] = np.linspace(mmin/mmax, 1.0, Ninterp)
        prob_q = mass_model.p_q(ddataset, beta=beta, mmin=mmin, delta_m=delta_m)
        sample_q = interp1d(np.cumsum(prob_q)/np.sum(prob_q), ddataset["mass_ratio"])
        get_probq = interp1d(ddataset["mass_ratio"], np.cumsum(prob_q)/np.sum(prob_q))
        
        m2 = m1*0.0
        for ii in range(m2.size):
            m2[ii] = sample_q(np.random.uniform(0.0, get_probq(m1[ii]/mmax)))*mmax
     
        return m1, m2
 
    def gaussian(self, params, Nsamples):
        mu = params["mu"] 
        sigma = params["sigma"] 
        
        m1 = norm.rvs(loc=mu, scale=sigma, size=Nsamples)
        m2 = norm.rvs(loc=mu, scale=sigma, size=Nsamples)

        return m1, m2 
 
    def uniform(self, params, Nsamples):
        um_min = params["um_min"] 
        um_max = params["um_max"] 
        
        mr_min = params["mr_min"] 
        mr_max = params["mr_max"] 
        
        nfac=1
        Ntot = 0
        
        m1n = np.array([], dtype=float)
        m2n = np.array([], dtype=float)
        
        while ( Ntot < Nsamples ):
            m1 = np.random.uniform(um_min, um_max, Nsamples*2)
            mass_ratio = np.random.uniform(mr_min, mr_max, Nsamples*2)
            m2 = mass_ratio*m1
            idx = (m2 > um_min) & (m2 < um_max)
            m1n = np.append (m1n, m1[idx])
            m2n = np.append (m2n, m2[idx])
            Ntot = len(m2n)

 
        return m1n[:Nsamples], m2n[:Nsamples]

 
    def loguniform(self, params, Nsamples):
        m1_min = params["m1_min"] 
        m1_max = params["m1_max"] 
        
        m2_min = params["m2_min"] 
        m2_max = params["m2_max"] 
        
        mr_min = params["mr_min"] 
        mr_max = params["mr_max"] 
 
        nfac=1
        Ntot = 0
        
        m1n = np.array([], dtype=float)
        m2n = np.array([], dtype=float)
        
        while ( Ntot < Nsamples ):
            m1 = 10**(np.random.uniform(np.log10(m1_min), np.log10(m1_max), Nsamples*2))
            mass_ratio = np.random.uniform(mr_min, mr_max, Nsamples*2)
            m2 = mass_ratio*m1
            idx = (m2 > m2_min) & (m2 < m2_max)
            m1n = np.append (m1n, m1[idx])
            m2n = np.append (m2n, m2[idx])
            Ntot = len(m2n)

 
        return m1n[:Nsamples], m2n[:Nsamples]


    def getMasses(self, params, Nsamples):
        if self.massmod == "SinglePeakSmoothedMassDistribution":
            return self.sample_m1_m2(params, Nsamples)
        elif self.massmod == "BNS_Gaussian":
            return self.gaussian(params, Nsamples)
        elif self.massmod == "BNS_Uniform":
            return self.uniform(params, Nsamples)
        elif self.massmod == "NSBH_LogUniform":
            return self.loguniform(params, Nsamples)
        else:
            raise NotImplementedError(f"Source mass model {self.massmod} not implemented")

       

class detFrameParams:
    def __init__(self):
        pass

    def getParams(self, NSamples):
        
        r = np.random.uniform(0,1,NSamples)
        ra = r*2*np.pi 

        r = np.random.uniform(0,1,NSamples)
        dec= (np.pi / 2.) - np.arccos( 2. * r - 1.) 

        r = np.random.uniform(0,1,NSamples)
        pol= r*2.*np.pi 

        r = np.random.uniform(-1,1,NSamples)
        inc = np.arccos(r) 
     #  r = np.random.uniform(0,1,NSamples)
     #  inc = r*2.*np.pi # inc

        t_gps = np.random.uniform(0,100000000,NSamples)
        
        return ra, dec, pol, inc, t_gps



class srcRateModel:
    def __init__(self,rzmod,R0):
        self.rzmod = rzmod

        ## Local Rate scaled to the correct units
        R0h67  = R0/Gpc3 # Mpc^3 / yr # Local value of merger rate
        self.R0s    = R0h67/h_H0**3 ### convert rate to h^3/Mpc^3/yr

    def MadauDickinson(self,zz,params):
      # """
      # Parameters
      # ----------
      # gamma: float
      #     Slope of the distribution at low redshift
      # kappa: float
      #     Slope of the distribution at high redshift
      # z_peak: float
      #     Redshift at which the distribution peaks.
      # """
      # md = MadauDickinsonRedshift()
      # return md.psi_of_z(zz, **params)
        aa=params["aa"]
        bb=params["bb"]
        zp=params["zp"]
        ##np.power(1.+zz,2.7)/(1.+np.power((1.+zs)/2.9,5.6))         
        return self.R0s*(1.+zz)**aa/(1. + ( (1.+zz)/(1.+zp) )**bb )         


    def Oguri(self,zz,params): 

        ## Oguri's fit to the Belczynski merger rate density R(z) ; Oguri et al. 2018
        ##a1, a2, a3, a4     = 6.6e3, 1.6, 2.1, 30
        a1=params["a1"]
        a2=params["a2"]
        a3=params["a3"]
        a4=params["a4"]
        RzOguriUnnormed    = lambda z: a1*np.exp(a2*z)/(np.exp(a3*z)+a4) /Gpc3 
        RzOguri            = lambda z: self.R0s * RzOguriUnnormed(z) / RzOguriUnnormed(0) 
        return RzOguri(zz)
    
    def RofzO3(self, zz, params): 
        gam=params["gamma"]
        kap=params["kappa"]
        zpk=params["zpk"]
        Fac = 1 + np.power( (1+zpk), (-gam-kap))
        denomfac = lambda z: np.power ( (1+z)/(1+zpk), (gam+kap) )
        RzO3 = lambda z: self.R0s * Fac * (1+z)**gam / ( 1 + denomfac(z))
        return RzO3(zz)
      
    def get_Rofz(self, zz, params=None):

        if self.rzmod == "MadauDickinson":
            if params == None:
                 params = {"R0": R0, "aa":2.7, "bb":5.6, "zp":1.9}
            return self.MadauDickinson(zz,params)

        elif self.rzmod == "Oguri":
            if params == None:
                 params = {"R0": R0, "a1":6.6e3, "a2":1.6, "a3":2.1, "a4":30}
            return self.Oguri(zz, params)

        elif self.rzmod == "RzO3":
            if params == None:
                 params = {"R0": R0, "kappa": 2.86, "gamma": 4.59, "z_peak": 2.47}
            return self.RzO3(zz, params)


class srcRedshiftModel:
    def __init__(self, params):
        self.sensitivity = params["sensitivity"]
        self.aum_cosm = params["aum_cosm"]
        self.zzmin = params["zsmin"]
        self.zzmax = params["zsmax"]
        self.R0 = params["R0"]
        self.rzmodel = params["rzmodel"] 
        self.rzparams = params["rzparams"] 
        self.sm = srcRateModel(self.rzmodel, self.R0)
        self.detlabel = params["detlabel"]
        self.srctype = params["srctype"]
     

    def init_snrspline(self):
        print("Calculating for ", self.sensitivity)
        fin = open("pickled_files/snr%s_%s_%s.pkl" % (self.sensitivity,self.detlabel, self.srctype), "rb")
        d = pickle.load(fin)
        fin.close()
        return d["zs"], d["splines"], d["maxsnr_spl"]
    
    
    def integrand_dn_zsrc(self, zs): 
        return 4*np.pi*self.aum_cosm.Dcofz(zs)**2 * self.sm.get_Rofz(zs,self.rzparams)/(1.+zs) * c/100./self.aum_cosm.Eofz(zs)
    
    def get_zsrc(self):
       #zs = np.linspace(0.15,10.,2000)
        zs = np.linspace(self.zzmin, self.zzmax, 1000)
        zsmin = self.zzmin; zsmax = zs[-1]
        z_tot = quad(self.integrand_dn_zsrc,zsmin,zsmax)[0]
        prob_z = zs*0.
        for ii in range(zs.size):
            prob_z[ii] = quad(self.integrand_dn_zsrc,zsmin,zs[ii])[0]/z_tot
            
        return UnivariateSpline(prob_z,zs,s=0)
    
    
    def get_zsrc_unlens(self):
        zs=np.linspace(self.zzmin, self.zzmax, 1000)
        zsmin=self.zzmin+(self.zzmax-self.zzmin)/1000.
        zsmax=zs[-1]
        z_tot=quad(self.integrand_dn_zsrc,zsmin,zsmax,args=(a))[0]
        prob_z = zs*0.
        for ii in range(zs.size):
            prob_z[ii]=quad(self.integrand_dn_zsrc,zsmin,zs[ii],args=(a))[0]/z_tot
            
        return UnivariateSpline(prob_z,zs,s=0)
     
                
