import sys
import numpy as np
from main import *
import lensprop as lp
import srcprop as sp

pars = {"sensitivity": sensitivity, "aum_cosm": aum_cosm, "zsmin": zsmin, "zsmax": zsmax, "R0": R0, "rzmodel": rzmodel, "rzparams": rzparams, "detlabel": detlabel, "srctype": srctype}

gpars = {"zlmin": zlmin, "zlmax":zlmax,  "vdmin":vdmin, "vdmax":vdmax, "axmin":axmin, "axmax":axmax, "aum_cosm": aum_cosm}

cpars = {"zlmin": zlmin, "zlmax":zlmax,  "Mmin":Mmin, "Mmax":Mmax, "aum_cosm":aum_cosm, "concpars": concpars}

sm = sp.srcRedshiftModel(pars)
rm = sp.srcRateModel(rzmodel, R0)
gp = lp.galaxyProperties(gpars)
cp = lp.clusterProperties(cpars) 

print("Lens params (zlmin,zlmax,vdmin,vdmax,axmin,axmax) = ", gpars["zlmin"],gpars["zlmax"],gpars["vdmin"],gpars["vdmax"],gpars["axmin"],gpars["axmax"])
print("Src params (zsmin,zsmax,rzmodel,R0) = ", pars["zsmin"], pars["zsmax"],pars["rzmodel"],pars["R0"])
#print("concentration params - scatter_sig, Mass_definition, mass-conc relation, cosmology",cpars["concpars"]) 


# Set lens and source parameters
def set_params(Ntot, shrflag, nseed):
    
    np.random.seed(nseed)
    # Get cumulative probability for lens redshift and velocity dispersion, ellipticity on a spline 
    # Using Padilla and Strauss 2008 distribution for axis ratio 
    # https://arxiv.org/abs/0802.0877
    zlspl = gp.get_zl()
    vdspl = gp.get_vd()
    qspl = gp.get_axratio()

    # Get cumulative probability for source redshift and SNR on a spline 
    zsspl = sm.get_zsrc()

    # Draw lens redshift, velocity dispersion, ellipticity and the position angle
    rand_zl = np.random.uniform(0, 1., Ntot)
    zlens = zlspl(rand_zl)

    rand_vd = np.random.uniform(0, 1., Ntot)
    gal_vd = vdspl(rand_vd)
   
    # Note axratio function returns axis ratio, we need ell where ell=1.-qq 
    rand_ell = np.random.uniform(0, 1., Ntot)
    gal_ell = 1.-qspl(rand_ell)
    gal_pa = np.random.uniform(-90., 90, Ntot)

    # Draw source redshifts and SNR independently
    zsprob = np.random.uniform(0, 1., Ntot)
    zsrc = zsspl(zsprob)

    snprob = np.random.uniform(0, 1., Ntot)
    uSNR = zsrc*0.

    zsarr, splinearr, z_snrmax_spl = sm.init_snrspline()
    sidx = np.searchsorted(zsarr, zsrc)
    print(Ntot,"rows to be generated")
    for ii in range(Ntot):
        if ii%1000 == 0 :
            print("Done with row",ii+1000)
        if 1:
            fac1 = splinearr["%.3f" % (zsarr[sidx[ii]])](snprob[ii])
            fac2 = z_snrmax_spl(zsrc[ii])
            fac1 = min(max(0, fac1), 1)
            uSNR[ii] = fac1 * fac2
        else:
            uSNR[ii] = 0

    # ellipticity and shear
    if(shrflag == 1):
        shear = np.random.uniform(0, 0.2, Ntot)
        sh_pa = np.random.uniform(-90., 90, Ntot)
        return zlens, gal_vd, gal_ell, gal_pa, shear, sh_pa, zsrc, uSNR
    # ellipticity and no shear
    else:
        return zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR
 

if __name__ == "__main__":
   
    # Draw the lens and source parameters
    Ntot = 10000000
    shrflag = 0   # 1 - with shear ; 0 - without shear
    nseed = np.random.seed(gseed+37000+np.int(nproc))  
    for sens in [sensitivity] :
        # With external shear, shear is not accounted for during the calculation of cross-section
        if shrflag == 1 :
           zlens, gal_vd, gal_ell, gal_pa, shear, sh_pa, zsrc, uSNR = set_params(Ntot, shrflag, nseed)
           np.savetxt("catalog_esh_%s_%s_%s.txt"%(sens, detlabel, srctype), np.transpose((zlens, gal_vd, gal_ell, gal_pa, shear, sh_pa, zsrc, uSNR)), fmt = '%e')
        else:
        # Without external shear
           zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR = set_params(Ntot, shrflag, nseed)
           np.savetxt("catalog_ell_%s_%s_%s.txt"%(sens, detlabel, srctype), np.transpose((zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR)), fmt = '%e')
     
