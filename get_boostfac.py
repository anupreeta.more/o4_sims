import numpy as np
from scipy.integrate import quad,fixed_quad
from scipy.interpolate import UnivariateSpline
from subprocess import call
import pandas
import srclensprop as slp
import os
from constants import *
import sys
from main import *

# Input label and processor number
lab = sensitivity  
rank = nproc  

np.random.seed(gseed+29000+rank)
fdir = "%s_%s_%s"%(lab, detlabel, srctype)


# No of sources to be chosen for a given lens for sufficient statistics; Note there is always 1 lens + 1 source. For simplicity, the lens is kept to be the same for N sources
df = pandas.read_csv("catalog_ell_%s.txt"%(fdir),sep=" ",header=None, names=(["zlens", "gal_vd", "gal_ell","gal_pa","zsrc","uSNR"]))

batch= rank*5000
batsize=batch+5000

# Read catalog with lens properties and source redshift, SNR
zlens, gal_vd, gal_ell, gal_pa, zsrc, uSNR=df.zlens.values[batch:batsize], df.gal_vd.values[batch:batsize], df.gal_ell.values[batch:batsize], df.gal_pa.values[batch:batsize], df.zsrc.values[batch:batsize], df.uSNR.values[batch:batsize]
 
# Read the maximum optical depth from the log file
tau_max=np.loadtxt("%s/tau_log.txt"%(fdir),usecols=([1]))

# Determine the number of lensed events given the lensing cross-section
tau=slp.optical_depth(zlens, gal_vd, 1.-gal_ell, zsrc)

# Choose the boost factor to be applied to augment the number of lenses
boost_fac = np.array([1e10,tau_max-(tau_max*0.1)])
tau_thresh = np.array([1e-10])

# Keep a log of the chosen values
if rank == 0 :
    print("## Boost_fac,tau_thresh, tau_max", boost_fac, tau_thresh, tau_max)

# Output file
fp=open("selcat_ind_%d.txt"%(nproc),"w")

for ii in range( zlens.size ):
    boostflag = -99
    # Num of lensed events behind a lens 
    if tau[ii] <= tau_thresh[0] : 
        numlensevt_boost = boost_fac[0]*tau[ii] 
        boostflag = 0
    elif tau[ii] > tau_thresh[0] :
        numlensevt_boost = boost_fac[1]*tau[ii]
        boostflag = 1
    elif tau[ii]*boost_fac[1] > 1 :
        print("Aborted. Boosted tau exceeds 1. Boost factor to be decreased")
        exit(0)

    # Store lens and source parameters in an output file
    lensflag=np.random.uniform(0,1)
    if( lensflag < numlensevt_boost and zlens[ii]*1.1 <= zsrc[ii] ):
        fp.write("%d %.3f %.2f %.3f %.2f %.2f %.3f %e %e %e %d\n"%(batch+ii,zlens[ii], gal_vd[ii], gal_ell[ii], gal_pa[ii], zsrc[ii],uSNR[ii],tau[ii],lensflag,numlensevt_boost, boostflag))
