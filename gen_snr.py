#!/usr/bin/env python
import numpy as np
import snr as snr
from main import *
import srcprop as sp
from constants import *

# nproc = nth processor on which the code will run,  sn_zbins = total redshift bins
np.random.seed(gseed+400+np.int(nproc))
print("Number of samples per processor are", NSamples)
if NSamples < 1000 :
    print("===========================WARNING======================== \n NSamples may NOT be sufficiently large and will result in sparser SNR distribution \n ======================================" )

# Generate and save network SNRs  as a function of source redshift from sn_zsmin to sn_zsmax ( limits are set in constants.py )
print("Generating SNR distribution in the redshift range:", sn_zsmin, sn_zsmax)
print("Running processor number:",nproc, "on a total of ", sn_zbins, " redshift bins")

# Binning in logspace to get more bins at lower redshifts which are more important
onepluszs= np.logspace(np.log10(1+sn_zsmin),np.log10(1+sn_zsmax),sn_zbins)
zs=onepluszs-1
whichz = int(nproc/sn_zbins)


# Initialise Mass Model
print("MassModel : ", massmodel)
mm=sp.srcMassModel(massmodel)
srcmasses=mm.getMasses(mm_params,NSamples)

# Initialise detector frame source params
dfp=sp.detFrameParams()
obsparams=dfp.getParams(NSamples)

# Generate the SNR distribution and store them in output files, see snr.py for details
sn=snr.SNR( sensitivity=[sensitivity], detectors=detectors, flow=flow, fhigh=fhigh, delta_f=delta_f, is_asd_file=True)
sn.getsnr(zs[whichz], nproc%sn_zbins, whichz, srcmasses, obsparams)

