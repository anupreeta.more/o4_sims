import numpy as np
from scipy.special import gamma
from scipy.interpolate import interp2d, UnivariateSpline
from scipy.integrate import quad,fixed_quad
from colossus.cosmology import cosmology
from colossus.halo import concentration


## Properties of Cluster-scale lens 
class clusterProperties:
    def __init__(self, params):
        self.zlmin = params["zlmin"]
        self.zlmax = params["zlmax"]
        self.Mmin = params["Mmin"]
        self.Mmax = params["Mmax"]
        self.aum_cosm = params["aum_cosm"]
 
    def get_halomassfunc(self, M200, zl): 
        massfunc = self.aum_cosm.nofm(M200, zl)  
        return massfunc
    
    def cumul_massfunc_zl(self, zl): 
        result = np.zeros(zl.size)
        for ii in range(zl.size):
            Dang = self.aum_cosm.Daofz(zl[ii])
            dVbydz = 4*np.pi*Dang**2 * (1+zl[ii])**2 * c/100./self.aum_cosm.Eofz(zl[ii])  
            ## Mhalo - min mass above which to integrate
            result[ii] = self.aum_cosm.Nplus(10**self.Mmin, zl[ii]) * dVbydz 
        return result 
    
    def getSpline_halo_zl(self):
        nlim = 100
        zl = np.linspace(self.zlmin, self.zlmax, nlim)
        zl_prob_tot = fixed_quad(self.cumul_massfunc_zl, self.zlmin, self.zlmax, n=20)[0]  
        zl_prob = np.zeros(zl.size)
        for ii in range(zl.size):
           zl_prob[ii] = fixed_quad(self.cumul_massfunc_zl, zl[ii], self.zlmax, n=20)[0]/zl_prob_tot 
        zl_prob_spl = interp1d(zl_prob, zl)
        return zl_prob_spl
    
    
    def getSpline_halo_zmass(self):
        Ntot = 50
        zl = np.linspace(self.zlmin, 1.5*self.zlmax, Ntot)
        mfp = np.zeros(Ntot*Ntot).reshape([Ntot, Ntot])
        zlp = np.zeros(Ntot*Ntot).reshape([Ntot, Ntot])
        mhp = np.zeros(Ntot*Ntot).reshape([Ntot, Ntot])
        for jj in range(zl.size):
            zlp[jj, :] = zl[jj]
            M200 = np.logspace(self.Mmin, self.Mmax, Ntot)
            for ii in range(M200.size):
                ## this is the M200mean
                mfp[jj][ii] = self.aum_cosm.Nplus(M200[ii], zl[jj])
                if ii == 0:
                    mfp0 = mfp[jj][0]
                mhp[:,ii] = M200[ii]
                mfp[jj][ii] /= mfp0
        halomass_zmf_spl = interp2d(zlp, np.log10(mfp), np.log10(mhp))
        return halomass_zmf_spl 
    
    
    def get_conc(self, M200m, zl, csig_scatter=10**0.1, Mdef="200m", model="diemer19", colossus_cosm="planck15"):
        cosmology.setCosmology(colossus_cosm)
        M200m = np.array(M200m)
        c200m_w_sig = np.zeros(M200m.size)
        for ii in range(M200m.size):
            c200m = concentration.concentration(10**M200m[ii], Mdef, zl[ii], model=model)
            # csig_scatter=10**0.1 is from Dutton and Maccio 2014 - http://adsabs.harvard.edu/abs/2014MNRAS.441.3359D
            csig = csig_scatter   
            c200m_w_sig[ii] = np.random.normal(loc=c200m, scale=csig)
        return  c200m_w_sig 
    
    
    def halo2lum(M200m):
        ## Eq. 6 and Table 5 of Cacciato (2013) - https://arxiv.org/abs/1207.0503 
        logM1 = 11.24
        logL0 = 9.95
        gam1 = 3.18
        gam2 = 0.245
        mrat = 10**(M200m-logM1)
        num = mrat**gam1
        denom = (1+mrat)**(gam1-gam2)
        return 10**logL0 * num / denom
    
    
    def lum2sigma(bcglum):
        ## Bernardi 2003, See eq. 14 of Oguri 2006
        bcg_sigma_vd = 10**2.197 * (bcglum/10**10.2 /h_H0**2)**1./4.
        return bcg_sigma_vd 


## Properties of Gal-scale lens 
class galaxyProperties: 
    def __init__(self, params):
        self.zlmin = params["zlmin"]
        self.zlmax = params["zlmax"]
        self.vdmin = params["vdmin"]
        self.vdmax = params["vdmax"]
        self.axmin = params["axmin"]
        self.axmax = params["axmax"]
        self.aum_cosm = params["aum_cosm"]
        

    def dnbydv(self, vdisp):
        ## See Choi, Park & Vogeley 2007; eq 4 from Oguri & Marshall 2010
        ## in units of 1/(hinv Mpc)^3
        phi_star = 8.0 * 1e-3
        vstar = 161
        alpha = 2.32
        beta = 2.67
        gamma_func = gamma(alpha/beta)
        return phi_star * (vdisp/vstar)**alpha * np.exp ( - (vdisp/vstar)**beta) * beta/gamma_func/vdisp
    
    
    def get_vd(self):
        nlim = 100
        vd = np.linspace(self.vdmin, self.vdmax, nlim)
        vdfunc_sum = fixed_quad(self.dnbydv, self.vdmin, self.vdmax, n=30)[0]
        vdfunc_cum = np.zeros(vd.size)
        
        for ii in range(vd.size):
           vdfunc_cum[ii] = quad(self.dnbydv, self.vdmin, vd[ii])[0]
        
        vdfunc_prob = vdfunc_cum/vdfunc_sum
        vdprob_spl = UnivariateSpline(vdfunc_prob, vd, s=0)
        return vdprob_spl
    
    def get_zl(self):
        nlim = 100
        zl = np.linspace(self.zlmin, self.zlmax, nlim)
        zl_prob = np.zeros(zl.size)
        for ii in range(zl.size):
           Dcom = self.aum_cosm.Dcofz(zl[ii])
           zl_prob[ii] = Dcom**3/(self.aum_cosm.Dcofz(self.zlmax)**3-self.aum_cosm.Dcofz(self.zlmin)**3)
        
        zlprob_spl = UnivariateSpline(zl_prob, zl, s=0)
        return zlprob_spl
    
    
    def init_axratio_spl(self):
       qq, pofq = np.loadtxt("padilla_strauss_catalog.dat", unpack=1)
       return UnivariateSpline(qq, pofq, s=0)
    
    def get_axratio(self):
       nlim = 100
       qqspl = self.init_axratio_spl()
       qq = np.linspace(self.axmin, self.axmax, nlim)
       qq_cum = qq*0.
       qmin = self.axmin
       qq_sum = quad(qqspl, qmin, qq[-1])[0]
       for ii in range(qq.size):
           qq_cum[ii] = quad(qqspl, qmin, qq[ii])[0]/qq_sum
       
       return UnivariateSpline(qq_cum, qq, s=0)
 
##def get_bcgsigma(M200):
##    ## Eq. 9 and 14 of Oguri 2006
##    ##  The Vale and Ostriker relation combined with the Faber Jackson relation from Bernardi et al. gives:
##    ##  0.73397 *(M_11)^p/(q+(M_11)^((p-r)*s) )^(1/s) = (vdisp/10^2.197)^4
##    ##  M11 is M/1e11 hinv Msun
##    ########################## XXXXX check this
##    M11=M200/1.e11  # in units of 1e11 hinv Msun
##    pp=4.
##    qq=0.57
##    rr=0.28
##    ss=0.23
##    denom= qq+M11**((pp-rr)*ss)
##    lum = 5.7*10**9*M11**pp / denom**(1./ss)
##    # km/s
##    bcg_sigma_vd=10**2.197 * (lum/10**10.2 /h_H0**2)**1./4.
##    return bcg_sigma_vd 
