# Planck15 cosmology taken from astropy - https://github.com/astropy/astropy/blob/53235d6b6e72d6404cc4a080daef7d45985cedb6/astropy/cosmology/data/Planck15.ecsv
H0,Om0,Tcmb0,Ob = ([67.74, 0.3075, 2.7255, 0.0486])
h_H0=H0/100.
ns,sigma8=([0.9667,0.8159])
Omk,w0,wa=([0,-1.0,0.0])

## Constants
kmpspMpctoGyr=977.813952
gee=4.2994e-9
c=299792.458

Gpc3   = 1.e9

## Used in gen_snr.py, mk_pkl.py
## Do not change unless really required 
## sn_zbins is tied to the NSamples in gen_snr.py, Queue in condor_snr.sub 
sn_zsmin = 0.001
sn_zsmax = 10.0
sn_zbins = 50

