#!/bin/bash

row=`awk -v var=$1 '{if(NR==var) print $NF}' "$2"`
imnum=`awk -v var=$1 '{if(NR==var) print $1}' "$2"`
echo "## random rownum, imnum" $1 $2 $row $imnum
awk  -v row=$row -v imnum=$imnum  -f get_improp.awk $3  >> $4 
