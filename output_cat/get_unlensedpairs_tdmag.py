#!/usr/bin/env python
import sys
import numpy as np

## for unlensed pairs
np.random.seed(9423)

def get_tdmag(fname, t0, tdur=3.1536e7, Ntot=100):
   td,dist= np.loadtxt(fname,usecols=(7,8),unpack=1)
   if(t0==0):
      t0=np.min(td)
      print("Taking t0 as",t0,"(sec)")
   else:
      print("Input t0 is",t0,"(sec)")
   
   idx = (td>=t0) & (td<=t0+tdur)
   td1 = td[idx]
   
   rand1=np.random.randint(0,td1.size,Ntot)
   rand2=np.random.randint(0,td1.size,Ntot)
 
   td_batch=np.abs((td[rand1]-td[rand2]))/3600./24.
   mag_batch=(dist[rand1]/dist[rand2])**2  ## magnification ratio Dlum propto 1/\sqrt(\mu)

   return td_batch, mag_batch

if(len(sys.argv)!=6):
    print("Usage: python code.py inp.txt 1 out.txt 10 6 0 \n \
        arg1: inp.txt, an input catalog of GW source params (m1,m2,zbbh,ra,dec,pol,inc,td,dist,snr) where snr could be network SNR or multiple columns for each detector \n \
        arg2: out.txt, output filename which will have the tdels and mu_rels \n \
        arg3: Number of pairs to generate \n \
        arg4: time duration of the observing run in days (max is 365 days)\n \
        arg5: t0 is the start of the observation in GPS format in seconds. 0 implies GPS start time is not known and arbitrary value will be taken")
    exit(0)
else:
    ## Input catalog name and output filenames
    fname = sys.argv[1]
    outname = sys.argv[2]
    Ntot = int(sys.argv[3])
    tdur = float(sys.argv[4])
    t0 = float(sys.argv[5])

data = get_tdmag(fname,t0,tdur*24*3600,Ntot)
np.savetxt(outname,np.transpose(data),fmt="%9.4f") 
