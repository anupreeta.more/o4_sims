import numpy as np
import sys

## for unlensed pairs
np.random.seed(9423)
import pylab as plt


## Input catalog name and output filenames
fname1="lensprop_gw_sn.txt"
fname2="imgprop_gw_sn.txt"
fref="../unlens_cat/unlens4lens_prop_O4_LHV_BBH_all.txt"
fout1="lensingmch_O4_LHV_BBH.pkl"

l_ID,l_sID,l_zbbh,l_uSNR,l_imno=np.loadtxt(fname1,usecols=(0,1,6,7,8),unpack=1)

im_ID,im_mag,im_tdel, im_par, im_sID=np.loadtxt(fname2,usecols=(0,3,4,5,6),unpack=1)

m1,m2,zbbh,ra,dec,pol,inc,tgps,dist,snrl,snrh,snrv=np.loadtxt(fref,unpack=1)


f_lid=[]; f_sid=[]; f_mg=[]; f_td=[];
f_m1=[];f_m2=[];f_zbbh=[];f_ra=[];f_dec=[]; f_pol=[];f_inc=[];f_tgps=[];f_dist=[];f_snrl=[];f_snrh=[];f_snrv=[];


SNRthresh=8.
netSNR= np.sqrt(snrl**2+snrh**2+snrv**2)
cntr=0
for ii in range(l_ID.size):
    indx1 =  (im_ID == np.int(l_ID[ii])) & (im_sID==np.int(l_sID[ii]))
    iid=im_ID[indx1]
    sid=im_sID[indx1]
    mg=im_mag[indx1]
    td=im_tdel[indx1]
    mag2sqrt= np.sqrt( sorted(np.abs(mg),reverse=True)[1])
    idx=((np.abs(l_zbbh[ii]-zbbh)<0.15) & (np.abs(l_uSNR[ii]-netSNR)<2.0) ) 

    if(zbbh[idx].size>0):
       ll=0
       while (ll<zbbh[idx].size and mag2sqrt*snrl[idx][ll]<=SNRthresh and mag2sqrt*snrh[idx][ll]<=SNRthresh and mag2sqrt*snrv[idx][ll]<=SNRthresh ):
           ll=ll+1
       if(ll<zbbh[idx].size):
           print(cntr, mag2sqrt*snrl[idx][ll], mag2sqrt*snrh[idx][ll], mag2sqrt*snrv[idx][ll], netSNR[idx][ll])
           cntr=cntr+1
           f_m1.append(m1[idx][ll])
           f_m2.append(m2[idx][ll])
           f_zbbh.append(zbbh[idx][ll])
           f_ra.append(ra[idx][ll])
           f_dec.append(dec[idx][ll])
           f_pol.append(pol[idx][ll])
           f_inc.append(inc[idx][ll])
           f_tgps.append(tgps[idx][ll])
           f_dist.append(dist[idx][ll])
           f_snrl.append(snrl[idx][ll])
           f_snrh.append(snrh[idx][ll])
           f_snrv.append(snrv[idx][ll])
           f_lid.append(iid)
           f_sid.append(sid)
           f_mg.append(mg)
           f_td.append(td)
          
import pickle

with open(fout1, 'wb') as handle:
    pickle.dump({ "lID": f_lid,
                  "sID": f_sid,
                  "m1": f_m1,
                  "m2": f_m2,
                  "zbbh": f_zbbh,
                  "ra": f_ra,
                  "dec": f_dec,
                  "pol": f_pol,
                  "inc": f_inc,
                  "tgps": f_tgps,
                  "dist": f_dist,
                  "snrl": f_snrl,
                  "snrh": f_snrh,
                  "snrv": f_snrv,
                  "mag": f_mg,
                  "tdel": f_td
                 }, handle, protocol=pickle.HIGHEST_PROTOCOL)


