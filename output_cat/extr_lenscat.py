import numpy as np
import random

sens="O4"
det="LHV"
src="BBH"
wdir="%s_%s_%s"%(sens,det,src)

dirp="../%s/out_fincat_%s"%(wdir,sens)

lid,imid,zl,vd,ell,pa,zs,uSNR,imno,junk1,junk2,bfac=np.loadtxt("%s/all_lensprop.txt"%(dirp),unpack=True)
qdid,qdx,qdy,qdmag,qdtdel,qdpar,srcqid=np.loadtxt("%s/quadall_imgprop.txt"%(dirp),unpack=True)
dbid,dbx,dby,dbmag,dbtdel,dbpar,srcdid=np.loadtxt("%s/dblall_imgprop.txt"%(dirp),unpack=True)

nevt=lid.size-2
rows=random.sample(range(1,lid.size),nevt)

lid=lid[rows]
imid=imid[rows]
zl=zl[rows]
vd=vd[rows]
ell=ell[rows]
pa=pa[rows]
zs=zs[rows]
uSNR=uSNR[rows]
imno=imno[rows]
bfac=bfac[rows]

fp1=open("imgprop_gw_sn.txt","w")
fp2=open("lensprop_gw_sn.txt","w")
flag=True
for ii in range(lid.size):
   dbflg=0; qdflg=0
   indx =  (qdid == np.int(lid[ii])) & (srcqid==np.int(imid[ii])) 
   indx1 = (dbid == np.int(lid[ii])) & (srcdid==np.int(imid[ii])) 
   if(np.sum(indx)==0):
       aa=dbid[indx1]
       bb=dbx[indx1]
       cc=dby[indx1]
       dd=dbmag[indx1]
       ee=dbtdel[indx1]
       ff=dbpar[indx1]
       gg=srcdid[indx1]

       if(flag): #np.sqrt(np.abs(dd[1]))*uSNR[ii]>5.):
           dbflg=1
           for jj in range(aa.size):
               fp1.write(" %d %s %s %s %s %s %d \n"%(aa[jj],bb[jj],cc[jj],dd[jj],ee[jj],ff[jj],gg[jj])) 
   else:
       aa=qdid[indx]
       bb=qdx[indx]
       cc=qdy[indx]
       dd=qdmag[indx]
       ee=qdtdel[indx]
       ff=qdpar[indx]
       gg=srcqid[indx]
       if(flag): #np.sqrt(np.abs(dd[1]))*uSNR[ii]>5.):
           qdflg=1
           for jj in range(aa.size):
               fp1.write(" %d %s %s %s %s %s %d \n"%(aa[jj],bb[jj],cc[jj],dd[jj],ee[jj],ff[jj],gg[jj])) 
         
   if(dbflg==1 or qdflg==1):
       fp2.write(" %d %d %.2f %.2f %.2f %.2f %.2f %.2f %d %d\n"%(lid[ii],imid[ii],zl[ii],vd[ii],ell[ii],pa[ii],zs[ii],uSNR[ii],imno[ii],bfac[ii]))
