#!/usr/bin/env python
import sys
import numpy as np

## for unlensed pairs
np.random.seed(9423)

def get_ulpairs(fname, flag, t0, tdur=3.1536e7, Ntot=100):
   if(flag == 1):
      m1,m2,zbbh,ra,dec,pol,inc,td,dist,netwsnr= np.loadtxt(fname,unpack=1)
   else:
      m1,m2,zbbh,ra,dec,pol,inc,td,dist,snrl,snrh,snrv= np.loadtxt(fname,unpack=1)
   if(t0==0):
      t0=np.min(td)
      print("Taking t0 as",t0,"(sec)")
   else:
      print("Input t0 is",t0,"(sec)")
   
   idx = (td>=t0) & (td<=t0+tdur)
   td1 = td[idx]
   
   rand1=np.random.randint(0,td1.size,Ntot)
   rand2=np.random.randint(0,td1.size,Ntot)
 
   print(rand1,rand2) 
   
   if(flag == 1): 
      return m1[rand1],m2[rand1],zbbh[rand1],ra[rand1],dec[rand1],pol[rand1],inc[rand1],td[rand1],dist[rand1],netwsnr[rand1], m1[rand2],m2[rand2],zbbh[rand2],ra[rand2],dec[rand2],pol[rand2],inc[rand2],td[rand2],dist[rand2],netwsnr[rand2]
   else:
      return m1[rand1],m2[rand1],zbbh[rand1],ra[rand1],dec[rand1],pol[rand1],inc[rand1],td[rand1],dist[rand1],snrl[rand1],snrh[rand1],snrv[rand1], m1[rand2],m2[rand2],zbbh[rand2],ra[rand2],dec[rand2],pol[rand2],inc[rand2],td[rand2],dist[rand2],snrl[rand2],snrh[rand2],snrv[rand2]


if(len(sys.argv)!=7):
    print("Usage: python code.py inp.txt 1 out.txt 10 6 0 \n \
        arg1: inp.txt, an input catalog of GW source params (m1,m2,zbbh,ra,dec,pol,inc,td,dist,snr) where snr could be network SNR or multiple columns for each detector \n \
        arg2: flag = 1 indicates the snr is 1 column whereas any value indicates 3 snr columns \n \
        arg3: out.txt, output filename which will have the tdels and mu_rels \n \
        arg4: Number of pairs to generate \n \
        arg5: time duration of the observing run in days (max is 365 days)\n \
        arg6: t0 is the start of the observation in GPS format in seconds. 0 implies GPS start time is not known and arbitrary value will be taken")
    exit(0)
else:
    ## Input catalog name and output filenames
    fname = sys.argv[1]
    flag = int(sys.argv[2])
    outname = sys.argv[3]
    Ntot = int(sys.argv[4])
    tdur = float(sys.argv[5])
    t0 = float(sys.argv[6])

data = get_ulpairs(fname,flag,t0,tdur*24*3600,Ntot)
np.savetxt(outname,np.transpose(data),fmt="%9.4f") 
