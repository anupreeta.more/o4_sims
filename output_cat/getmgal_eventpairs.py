import numpy as np
import pylab as plt
import scipy.stats as st
import getmgal as gm
import snronpairs_quad_dbl as sn
import sys

## Network detector - SNR threshold; This can only be set to higher than 8 currently because the simulated lensed pair catalog is generated for the brightest pairs of a lens system to with lensed SNRs > 8. Systems with lower network SNRs are not stored.
SNRthresh=8

if(len(sys.argv)!=7):
    print("Usage: python getmgal_eventpairs.py gw_inp.txt ulpairs_tdmag.txt 0 O4 mgal_out.txt \n \
	arg1: gw_inp.txt, a catalog of (mock or real) event pairs (evt1ID, evt2ID, tdel, mu_rel) where tdel is in seconds and whose Mgal needs to be calculated \n \
	arg2: unlensed pair catalog with tdel and mu_rel only \n \
	arg3: directory path to mock lensed samples e.g. O4_LHV_BBH or Ap_LHVKI_BNS \n \
	arg4: O3 or O4 observing run \n \
	arg5: 0 or 1 where 0 implies event pairs with phase difference of 0 deg or 1 implies phase difference of 90deg \n \
	arg6: mgal_out.txt is the output filename where Mgal values will be stored")
    exit(0)
else: 
    gwdatf = sys.argv[1] 
    ulpairs = sys.argv[2]
    dirp = sys.argv[3]
    det = sys.argv[4]
    flag_phdiff = int(sys.argv[5])
    fout = sys.argv[6]

## Simulated lensed pairs used in generating the KDE
mag31,tdel31,mag32,tdel32,mag41,tdel41,mag42,tdel42,mag21,tdel21,mag43,tdel43,dbmg21,dbtd21= sn.getsnr_forpairs(dirp,det,SNRthresh)

if flag_phdiff == 0 :
    ## phase diff of 0
    tdel= np.concatenate([tdel21,tdel43])
    mrat= np.concatenate([mag21,mag43])
elif flag_phdiff == 1 :
    ## phase diff of 90
    tdel= np.concatenate([tdel31,tdel32,tdel41,tdel42,tdel21,tdel43,dbtd21])
    mrat= np.concatenate([mag31,mag32,mag41,mag42,mag21,mag43,dbmg21])

## Simulated unlensed pairs used in generating the KDE
tdu,magu=np.loadtxt(ulpairs,unpack=1) 
idxu=(tdu>1e-3) & (magu>1e-3) 

## GW events difference of gps times; in seconds
gwtdel,gwrmag=np.loadtxt(gwdatf,usecols=(2,3),unpack=1)
gwevt1,gwevt2=np.loadtxt(gwdatf,dtype=str,usecols=(0,1),unpack=1)

## GW events time is converted from seconds to days
gwt=gwtdel/3600/24
gwm=gwrmag

## Lensed hypothesis  
kernl=gm.getMgal(tdel,mrat) 
HL=kernl([np.log10(gwt),np.log10(gwm)]) 

## Unlensed hypothesis  
kernu=gm.getMgal(tdu[idxu],magu[idxu])
HU=kernu([np.log10(gwt),np.log10(gwm)]) 

## Taking the brightest two events and then calculating Mgal - equivalent to Haris
print("The phase difference is 0") if flag_phdiff == 0 else print("The phase difference is 90")

np.savetxt(fout,np.transpose([gwevt1,gwevt2,HL,HU, np.log10(HL/HU)]), header="Evt1ID, Evt2ID, HL, HU, log10(Mgal)", fmt="%s")
