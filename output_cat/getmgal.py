import numpy as np
import scipy.stats as st

def getMgal(xdat,ydat):

    # Extract x and y 
    xu=np.log10(xdat)
    yu=np.log10(ydat)

    xmin = np.log10(1e-2) 
    xmax = np.log10(5e2)  
    ymin = np.log10(1e-2)    
    ymax = np.log10(1e2)  
    
    xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([xu, yu])
    kernel = st.gaussian_kde(values)
    return kernel

def getMgal1D(xdat):

    # Extract x and 
    xu=np.log10(xdat)

    xmin = np.log10(1e-2) 
    xmax = np.log10(5e2)  
    
    xx = np.mgrid[xmin:xmax:100j]
    positions = np.vstack([xx.ravel()])
    values = np.vstack([xu])
    kernel = st.gaussian_kde(values)
    return kernel

