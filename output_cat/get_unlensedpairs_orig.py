import numpy as np
import sys

## for unlensed pairs
np.random.seed(9423)
import pylab as plt

def get_tdmag(fname, nn, Ntot, flag, batchtime=3.1536e7):
   if(flag == 1 ):
      m1,m2,zbbh,ra,dec,pol,inc,td,dist,snrl,snrh,snrv= np.loadtxt(fname,unpack=1)
   else:
      m1,m2,zbbh,ra,dec,pol,inc,td,dist,netwsnr= np.loadtxt(fname,unpack=1)
   
   idx = (td>=(nn)*batchtime) & (td<=(nn+1)*batchtime)
   td1 = td[idx]
   
   rand1=np.random.randint(0,td1.size,Ntot)
   rand2=np.random.randint(0,td1.size,Ntot)
 
   print(rand1,rand2) 
   
   if(flag == 1): 
      return m1[rand1],m2[rand1],zbbh[rand1],ra[rand1],dec[rand1],pol[rand1],inc[rand1],td[rand1],dist[rand1],snrl[rand1],snrh[rand1],snrv[rand1], m1[rand2],m2[rand2],zbbh[rand2],ra[rand2],dec[rand2],pol[rand2],inc[rand2],td[rand2],dist[rand2],snrl[rand2],snrh[rand2],snrv[rand2]
   else:
      return m1[rand1],m2[rand1],zbbh[rand1],ra[rand1],dec[rand1],pol[rand1],inc[rand1],td[rand1],dist[rand1],netwsnr[rand1], m1[rand2],m2[rand2],zbbh[rand2],ra[rand2],dec[rand2],pol[rand2],inc[rand2],td[rand2],dist[rand2],netwsnr[rand2]


## Input catalog name and output filenames
fname = sys.argv[1]
outname = sys.argv[2]
flag = sys.argv [3]


delt = 3.1536e7/1. ## 12 months for O4 observing run
det = np.array(["O4","AL"])
batchnum = np.array([1,3])
batchtime = np.array([delt,delt])
Ntot = 50000
tdcomb = np.zeros(Ntot)
magcomb = np.zeros(Ntot)
## Running for O4 only
for ii in range(det.size-1):
   batchsize = np.int(Ntot/batchnum[ii])
   for jj in range(batchnum[ii]):
      mini = jj*batchsize 
      maxi = (jj+1)*batchsize 
      data = get_tdmag(fname,batchnum[ii],batchsize,flag,batchtime[ii])
  
   np.savetxt(outname,np.transpose(data),fmt="%9.4f") 
