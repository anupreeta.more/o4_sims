## $1,$2,$3 are sensitivity, detectors and srctype e.g. O4 LHVK BBH
cp snrlog/0.snout $1_$2_$3"/snr_log.txt"

mkdir "netwsnr_"$1  "snr_indiv_"$1
rm run_netwsnr1   
for ii in {0..49}
    do
        foo=$(printf "Combining individual Output*txt files for redshift bin: %02d" $ii)
        echo "cat Output_"$1"_"$ii"_netsnr_* > netwsnr_"$1"/netsnr_"$foo".txt" >> run_netwsnr1
    done

echo "Storing SNR distributions for each redshift bin in netwsnr_*"
sh run_netwsnr1
echo "Moving individual Output*txt files to snr_indiv_*"
mv Output*_netsnr_*txt "snr_indiv_"$1"/"

mv "netwsnr_"$1  "snr_indiv_"$1  $1_$2_$3"/"

echo "Making pickle file for SNRs in pickled_files"
python3  ./mk_pkl.py $1 $2 $3
