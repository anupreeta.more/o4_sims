#!/usr/bin/env python
import sys
import numpy as np
import pycbc.noise
from pycbc.detector import Detector
import pycbc.psd
import pycbc.filter
import pycbc.waveform
from astropy.cosmology import Planck15
from scipy.interpolate import interp1d
from constants import *


class SNR:
    def __init__(self,  sensitivity=["O4"], detectors=["L1", "H1", "V1"], flow=20, fhigh=2048, delta_f=1./4., is_asd_file=True):
        print("Setting : ", sensitivity, detectors)
        self.detectors = detectors
        self.flow = flow
        self.fhigh = fhigh
        self.delta_f = delta_f
        self.flen = int(fhigh / delta_f) + 1
        self.PSDs = self.getPSDs(sensitivity, is_asd_file)
        print("flow, fhigh, delta_f, flen : ", flow, fhigh, delta_f, self.flen )

    def getPSDs(self, sensitivity, is_asd_file):
        PSDs={}
        PSD_det={}
        for sens in sensitivity:
           print("Calculating psd for ", sens)
           if sens == "O3" :
                for detector in self.detectors:
                    psdfile="PSDs/O3PSD_%s.txt"%(detector)
        ###### DEBUG THIS #########################
                    psd=pycbc.psd.read.from_txt(psdfile,
                                                self.flen,
                                                low_freq_cutoff=self.flow,
                                                delta_f=self.delta_f,
                                                is_asd_file=is_asd_file)
                    freq=psd.get_sample_frequencies()
                    PSD_det[detector]=psd

                PSDs[sens]=PSD_det
           if sens == "O4" :
                for detector in self.detectors:
                    psdfile="PSDs/O4PSD_%s.txt"%(detector)
                    psd=pycbc.psd.read.from_txt(psdfile,
                                                self.flen,
                                                low_freq_cutoff=self.flow,
                                                delta_f=self.delta_f,
                                                is_asd_file=is_asd_file)
                    freq=psd.get_sample_frequencies()
                    PSD_det[detector]=psd

                PSDs[sens]=PSD_det
           elif sens == "AL":
                AdVL={}
                for detector in self.detectors:
                    if detector == "L1" or detector == "H1" or detector == "I1"  :
                        AdVL[detector]  =  pycbc.psd.analytical.aLIGODesignSensitivityP1200087(self.flen, self.delta_f, self.flow)
                    elif detector == "V1" : 
                        AdVL[detector]  =  pycbc.psd.analytical.AdvVirgo(self.flen, self.delta_f, self.flow)
                    elif detector == "K1" : 
                        AdVL[detector]  =  pycbc.psd.analytical.KAGRADesignSensitivityT1600593(self.flen, self.delta_f, self.flow)
                    else:
                         print("Detector not known. Try LHV, LHVK or LHVKI")
                         continue
                PSDs[sens] = AdVL
               
           elif sens == "Ap":

                psdfile="PSDs/AplusDesign.txt"
                psd=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                psdfile="PSDs/avirgo_O5high_NEW.txt"
                psdv=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                psdfile="PSDs/kagra_128Mpc.txt"
                psdk=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                Aplus = {}
                for detector in self.detectors:
                    if detector == "L1" or detector == "H1"  or detector == "I1" :
                        Aplus[detector]  = psd
                    elif detector == "V1" : 
                        Aplus[detector]  = psdv
                    elif detector == "K1" : 
                        Aplus[detector]  =  psdk
                    else:
                         print("Detector not known. Try LHV, LHVK or LHVKI")
                         continue
                PSDs[sens] = Aplus
                
           elif sens == "Vo":
                Vo_psd = {}
                psdfile="PSDs/voyager.txt"
                psd=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                psdfile="PSDs/avirgo_O5high_NEW.txt"
                psdv=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                psdfile="PSDs/kagra_128Mpc.txt"
                psdk=pycbc.psd.read.from_txt(psdfile,
                                            self.flen,
                                            low_freq_cutoff=self.flow,
                                            delta_f=self.delta_f,
                                            is_asd_file=is_asd_file)
                freq=psd.get_sample_frequencies()
                for detector in self.detectors:
                    if detector == "L1" or detector == "H1"  or detector == "I1" :
                        Vo_psd[detector] = psd
                    elif detector == "V1" :
                        Vo_psd[detector] = psdv 
                    elif detector == "K1" :
                        Vo_psd[detector] = psdk
                    else:
                         print("Detector not known. Try LHV, LHVK or LHVKI")
                         continue
                PSDs[sens] = Vo_psd

           elif sens == "CE":
                CE_psd={}
                for detector in self.detectors:
                    if detector == "L1" or detector == "H1"  or detector == "X1" :
                         CE_psd[detector]=pycbc.psd.analytical.CosmicExplorerP1600143(self.flen, self.delta_f, self.flow)
                    else:
                         print("Detector not known. Try LH or X")
                         continue
                PSDs[sens] = CE_psd

           elif sens == "ET":
                ET_psd={}
                for detector in self.detectors:
                    if detector == "E1" or detector == "E2"  or detector == "E3" :
                        ET_psd[detector]=pycbc.psd.analytical.EinsteinTelescopeP1600143(self.flen, self.delta_f, self.flow)
                    else:
                         print("Detector not known. Try 3ET")
                         continue
                
                PSDs[sens] = ET_psd

           elif sens == "EC":
                EC_psd={}
                for detector in self.detectors:
                    if detector == "E1" or detector == "E2"  or detector == "E3" :
                        EC_psd[detector]=pycbc.psd.analytical.EinsteinTelescopeP1600143(self.flen, self.delta_f, self.flow)
                    elif detector == "X1" :
                        EC_psd[detector]=pycbc.psd.analytical.CosmicExplorerP1600143(self.flen, self.delta_f, self.flow)
                    else:
                         print("Detector not known. Try 4EC")
                         continue
                
                PSDs[sens] = EC_psd


        return PSDs

    def innerProduct(self, h1, h2, df, psd):
        result = 4.*np.real( np.conjugate(h1.data)*h2.data / psd.data )*df
        result[np.isnan(result)|(result>1e20)] = 0
        return np.sum(result)

    def getsnr(self, zsrc, iterval, zzbin, srcmass, obsparams):
        m1, m2 = srcmass 
        m1z = (1+zsrc)*m1
        m2z =(1+zsrc)*m2
        NSamp=m1z.size
        ra,dec,pol,inc,t_gps = obsparams
        distances = Planck15.luminosity_distance(zsrc).value
        innerProductsPlus  = {}
        innerProductsCross = {}
        for sensitivity in self.PSDs.keys():
            innerProductsPlus[sensitivity]  = {}
            innerProductsCross[sensitivity] = {}
            for detector in self.PSDs[sensitivity].keys():
                innerProductsPlus[sensitivity][detector]  = np.zeros(NSamp)
                innerProductsCross[sensitivity][detector] = np.zeros(NSamp)
                psd = self.PSDs[sensitivity][detector]
                for i in range(NSamp):
                    hp, hc    = pycbc.waveform.get_fd_waveform(approximant="IMRPhenomPv2", mass1=m1z[i], mass2=m2z[i], f_lower=self.flow, delta_f=self.delta_f, inclination=inc[i], distance=distances, f_final = self.fhigh)
                    innerProductsPlus[sensitivity][detector][i]  = self.innerProduct(hp, hp, self.delta_f, psd)
                    innerProductsCross[sensitivity][detector][i] = self.innerProduct(hc, hc, self.delta_f, psd)
        
        
        Fp, Fc = {}, {}
        for det in self.detectors:
            # Detectors 
            ddet = Detector(det)
            Fp[det], Fc[det] = ddet.antenna_pattern(ra, dec, pol, t_gps=t_gps)
        
        SNRs = {}
        for sensitivity in self.PSDs.keys():
            networkSNR = []
            SNRs[sensitivity] = {}
           #for detector in self.PSDs[sensitivity].keys():
            for detector in self.detectors:
                SNRs[sensitivity][detector] = np.sqrt(Fp[detector]**2*innerProductsPlus[sensitivity][detector] + Fc[detector]**2*innerProductsCross[sensitivity][detector])
                SNR = SNRs[sensitivity][detector]
                networkSNR.append(SNR)
            nSNR = np.sqrt(np.sum(np.array(networkSNR)**2,axis=0))
            print("Saving Output_%s_%d_netsnr_%02d.txt"%(sensitivity,zzbin, iterval))
            np.savetxt("Output_%s_%d_netsnr_%02d.txt"%(sensitivity,zzbin, iterval), np.transpose([nSNR]))


    def getsnr_unlens(self, zsrc, rank, detlabel, srctype, srcmass, obsparams, SNRflag, SNRthresh):
        m1, m2 = srcmass 
        m1z = (1+zsrc)*m1
        m2z =(1+zsrc)*m2
        NSamp=m1z.size
        ra,dec,pol,inc,t_gps = obsparams
        distances = Planck15.luminosity_distance(zsrc).value
        innerProductsPlus  = {}
        innerProductsCross = {}
        for sensitivity in self.PSDs.keys():
            innerProductsPlus[sensitivity]  = {}
            innerProductsCross[sensitivity] = {}
            for detector in self.PSDs[sensitivity].keys():
                innerProductsPlus[sensitivity][detector]  = np.zeros(NSamp)
                innerProductsCross[sensitivity][detector] = np.zeros(NSamp)
                psd = self.PSDs[sensitivity][detector]
                for i in range(NSamp):
                    hp, hc    = pycbc.waveform.get_fd_waveform(approximant="IMRPhenomPv2", mass1=m1z[i], mass2=m2z[i], f_lower=self.flow, delta_f=self.delta_f, inclination=inc[i], distance=distances[i], f_final = self.fhigh)
                    innerProductsPlus[sensitivity][detector][i]  = self.innerProduct(hp, hp, self.delta_f, psd)
                    innerProductsCross[sensitivity][detector][i] = self.innerProduct(hc, hc, self.delta_f, psd)
        
        Fp, Fc = {}, {}
        detnum = 0
        for det in self.detectors:
            # Detectors 
            ddet = Detector(det)
            Fp[det], Fc[det] = ddet.antenna_pattern(ra, dec, pol, t_gps=t_gps)
            detnum += 1
        # This loop stores unlensed GW source properties
        SNRs = {}
        if ( SNRflag == 0 ): 
            for sensitivity in self.PSDs.keys():
                networkSNR = []
                SNRs[sensitivity] = {}
               #for detector in self.PSDs[sensitivity].keys():
                for detector in self.detectors:
                    SNRs[sensitivity][detector] = np.sqrt(Fp[detector]**2*innerProductsPlus[sensitivity][detector] + Fc[detector]**2*innerProductsCross[sensitivity][detector])
                    SNR = SNRs[sensitivity][detector]
                    networkSNR.append(SNR)
                nSNR = np.sqrt(np.sum(np.array(networkSNR)**2,axis=0))
                # signals with network SNR above SNRthresh=8 are stored 
                indx=nSNR>SNRthresh
                fout="unlens_det_prop_%s_%s_%s_%d.txt"%(sensitivity, detlabel, srctype, rank)
                print("Saving detectable unlensed source params in",fout,"where network SNR threshold is",SNRthresh)
                np.savetxt(fout, np.transpose([m1[indx], m2[indx], zsrc[indx], ra[indx], dec[indx], pol[indx], inc[indx], t_gps[indx], distances[indx], nSNR[indx]]), fmt="%.5e") 

        elif ( SNRflag > 0 ): 
            if ( SNRflag == 1 ): 
                #  GW signals with at least 1-detector SNR is above SNR threshold=8
                fout="unlens_sngdet_prop_%s_%s_%s_%d.txt"%(sensitivity, detlabel, srctype, rank)
                print("Saving unlensed source params (with at least 1 detector SNR is detectable) in",fout,"where single detector SNR threshold is",SNRthresh )
                fp=open(fout,"w")
            elif ( SNRflag == 2 ): 
                # all GW signals are stored to which lensing will be applied 
                fout="unlens4lens_prop_%s_%s_%s_%d.txt"%(sensitivity, detlabel, srctype, rank)
                print("Saving all (without SNR detectability criteria) unlensed source params in",fout)
                fp=open(fout,"w")
            SNRarr=np.zeros(NSamp*detnum).reshape(detnum,NSamp)
            for sensitivity in self.PSDs.keys():
                SNRs[sensitivity] = {}
                dd=0
               #for detector in PSDs[sensitivity].keys():
                for detector in self.detectors:
                    SNRs[sensitivity][detector] = np.sqrt(Fp[detector]**2*innerProductsPlus[sensitivity][detector] + Fc[detector]**2*innerProductsCross[sensitivity][detector])
                    SNRarr[dd] = SNRs[sensitivity][detector]
                    dd=dd+1
                for ii in range(NSamp):
                    if ( SNRflag == 1 ): 
                        indx = SNRarr[:,ii] > SNRthresh
                    if ( SNRflag == 2 or len(SNRarr[indx,ii]) > 0): 
                        fp.write("%.5e %.5e %.5e %.5e %12.5e %.5e %.5e %.5e %.5e "%(m1[ii], m2[ii], zsrc[ii], ra[ii], dec[ii], pol[ii], inc[ii], t_gps[ii], distances[ii]))
                        for jj in range(detnum):
                            fp.write(("%.5e "%(SNRarr[jj][ii])))
                        fp.write("\n")

#for detector in self.PSDs[sensitivity].keys():
#    SNR = np.sqrt(Fp[detector][ii]**2*innerProductsPlus[sensitivity][detector][ii] + Fc[detector][ii]**2*innerProductsCross[sensitivity][detector][ii])
#    fp.write("%.4f "%(SNR))
#fp.write("\n")
if __name__ == "__main__":

    sensitivity=["Vo"]; detectors=["L1", "H1", "V1", "K1", "I1"]; flow=20; fhigh=2048; delta_f=1./4.; is_asd_file=True;
    sn=SNR(sensitivity, detectors, flow, fhigh, delta_f, is_asd_file)
    psd_netw=sn.getPSDs(sensitivity, is_asd_file)
    import pylab as plt 
    for kk in range(len(sensitivity)):
        print(psd_netw[sensitivity[kk]][detectors[0]])
        freq=psd_netw[sensitivity[kk]][detectors[0]].sample_frequencies
        for ii in range(len(detectors)) :
            print(sensitivity[kk],ii,detectors[ii])
            plt.plot(freq,psd_netw[sensitivity[kk]][detectors[ii]],label=detectors[ii])
            plt.yscale("log")
            plt.ylim(10**-50, 10**-35)
            plt.legend()
        plt.savefig("check_psds_%s.pdf"%(sensitivity[kk]))
        plt.clf()
