
## Introduction

The purpose of the code is to generate distributions of lensed image properties (positions, magnifications, time delays, image types) upon specifying the lens models, source models, detector combinations, sensitivities and SNR conditions with thresholds.

## Argument options

See options in "main.py"

1. Lens type are Gal (default);  TBA: Groups/Clus

Lens mass functions are given by SDSS velocity dispersion function (default for gal), Halo Mass Function (default for groups/clus)

Lens mass models are given by SIE (default); TBA:SIE+NFW, PowLaw

--lensargs : List of arguments include setting (min, max) of lens + source redshifts, axis ratio, velocity dispersion + halo mass


2. Source types can be chosen from binary black holes/neutron stars or neutron star-black holes

--srclabel : Options include BBH (default), BNS and NSBH

3. Source mass distributions can be chosen from Power Law+Peak (BBH), Gaussian and Uniform (BNS), LogUniform (NSBH)

--massmodel : Options include BBH_PowLawPeak (default), BNS_Gaussian, BNS_Uniform, NSBH_LogUniform

4. Source rate models can be chosen from Madau Dickinson, Oguri+Belczynski (Oguri 2018), Best-fit PowLaw+Peak from O3 ()

--ratemodel : Options include MadauDickinson (default), Oguri, RzO3

--localrate : Specify local merger rate R0 Gpc^3/yr

5. Detectors can be chosen from LIGO/Virgo, Voyager, Cosmic Explorer, Einstein Telescope

--detectors : Options include LHV (default), LHVK, LHVKI, CE(X or 3CE), 3ET(ET1,ET2,ET3), 4EC (ET1,ET2,ET3,X1)

6. PSD sensitivities can be chosen from O3, O4 (default), AdvLIGO,  Aplus, Vo, CE, ET, EC (CE+ET)

--sensitivity : Options include O3, O4, AL, Ap, Vo, CE, ET or EC

7. SNR related thresholds and conditions are i) SNRflag for conditions to be applied to a network of detectors, single detector or no detectability criteria to be applied, ii) SNRthresh, the actual value of the threshold to be applied on the SNR as measured in the detector(s) and iii) NSamples, the number of samples to be generated per processor. One may reduce from the default value of 5000 but, if reduced substantially, then sampling of the SNR distribution will be poor.

--snrargs : arguments include SNRflag SNRthresh NSamples




## Required packages

1. **glafic**  - for solving lens equation

Binary code downloaded from - https://www.slac.stanford.edu/~oguri/glafic/

Manual: https://github.com/oguri/glafic2/blob/main/manual/man_glafic.pdf

Reference:

M. Oguri, PASJ, 62, 1017 (2010)

2010ascl.soft10012O

FYI, latest code (in python): https://github.com/oguri/glafic2/

2. **aum**  - for mass function, angular diameter distances

Installation on CIT / LIGO clusters:   pip install git+https://github.com/surhudm/aum.git

See other ways to install:  http://surhudm.github.io/aum/install.html

Source code: https://github.com/surhudm/aum

Manual: http://surhudm.github.io/aum/

Reference:

arXiv:1206.6890

https://ascl.net/2108.002

3. **colossus** - for c-M relation of clusters

Installation: pip install colossus

See more details here - https://bdiemer.bitbucket.io/colossus/installation.html

Manual: https://bdiemer.bitbucket.io/colossus/

Reference: Diemer et al. 2017 (arxiv:1712.04512)

4. **condor** - job submissions and queuing system for multi-node servers

LIGO clusters already have condor.

Admin permissions required. Not recommended for individual users.

Installation: https://htcondor.readthedocs.io/en/latest/man-pages/condor_install.html

Manual: https://htcondor.readthedocs.io/en/latest/man-pages/index.html



## Installation

git clone git@git.ligo.org:anupreeta.more/o4_sims.git

or

git clone https://git.ligo.org/anupreeta.more/o4_sims.git


## Usage

See the step-by-step instructions given in the shell script -
 run_main
