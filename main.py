import numpy as np
import sys
##sys.path.append("/home/anupreeta.more/soft/aum/install/lib/python3.7/site-packages")
##sys.path.append("/Users/amore/soft/aum/install/lib/python3.7/site-packages")
import cosmology as cc
from constants import *

# Initialising cosmology for mass function, distances, etc. 
aum_cosm = cc.cosmology(Om0,Omk,w0,wa,Ob,h_H0,Tcmb0,sigma8,ns,np.log10(8.0),1.0)
## Planck15, Table 4, col 4,  TT,TE,EE+lowP or Table 3 col 4
#aum_cosm = cc.cosmology(0.3156,0.0,-1.0,0.0,0.04917,h_H0,2.726,0.831,0.9645,np.log10(8.0),1.0)

import argparse

parser = argparse.ArgumentParser(description="Generates the lensed image distributions for different lens models, source models, detectors, sensitivities and SNR conditions")
parser.add_argument('--sensitivity', help = "Specify detector sensitivity  e.g. O3, O4 (default), AL, Ap, Vo, CE, ET or EC", default = "O4")
parser.add_argument('--detectors', help = "Specify the names of the detectors LHV (default), LHVK, LHVKI, CE(X or 3CE) ,3ET(ET1,ET2,ET3), 4EC (ET1,ET2,ET3,X1)",  default = "LHV")
parser.add_argument('--srclabel', help = "Specify the type of source e.g. BBH (default), BNS, NSBH", default = "BBH")
parser.add_argument("--psdfreqargs", help = "Specify the flow, fhigh and delta_f in Hz", type = float, nargs = 3, default = (20, 2048, 0.25))
parser.add_argument('--globalseed', help = "Global seed that sets other seeds", type=int, default = 777)
parser.add_argument('--snrargs', help = "Unlensed source SNR related arguments. 1. SNRflag (0 =  network SNR > SNRthresh, 1 = at least one detector has SNR> SNRthresh and 2 = all sources (without detectability criteria) will be stored) 2. SNRthresh - snr threshold (e.g. network SNR of 8)  3. NSamples to calculate per processor (e.g. default=5000)  ", type=int, nargs=3, default = (1, 8, 5000))
parser.add_argument('--massmodel', help = "Specify binary mass model e.g. BBH_PowLawPeak (default), BNS_Gaussian, BNS_Uniform, NSBH_LogUniform", default = "BBH_PowLawPeak")
parser.add_argument('--ratemodel', help = "Specify merger rate density model e.g. MadauDickinson (default), Oguri, RzO3", default = "MadauDickinson")
parser.add_argument('--localrate', help = "Specify the local merger rate R0 Gpc^3/yr ", type=float, default = 1.)
parser.add_argument("--condornproc", help = "Specify the number of processors. Mainly required for condor. It can be set to 0 otherwise", type=int, default = 0)
parser.add_argument("--lensargs", help = "Specify the lens_src params (zlens_min,zlens_max,zsrc_min,zsrc_max,veldisp_min,veldisp_max,axratio_min,axratio_max,Mhalo_min,Mhalo_max", type = float, nargs = 10)
parser.add_argument("--concargs", help = "Specify the concentration params (log(sigma_scatter),Massdef,mass-conc relation,cosmology). Needed only for cluster-scale lenses", nargs = 4)

args = parser.parse_args()

sensitivity = args.sensitivity

detlabel = args.detectors
if detlabel == "LH":
    detectors = ["L1", "H1"] 
elif detlabel == "LHV":
    detectors = ["L1", "H1", "V1"] 
elif detlabel == "LHVK":
    detectors = ["L1", "H1", "V1", "K1"] 
elif detlabel == "LHVKI":
    detectors = ["L1", "H1", "V1", "K1", "I1"] 
elif detlabel == "3ET":
    detectors = ["E1", "E2", "E3"] 
elif detlabel == "4EC":
    detectors = ["E1", "E2", "E3", "X1"] 
elif detlabel == "3CE":
    detectors = ["L1", "H1", "X1"] 
elif detlabel == "X":
    detectors = ["X1"] 
else:
    print(detlabel," is unknown")
    exit(0)

srctype = args.srclabel

# Frequency, sampling rate, length settings
flow = args.psdfreqargs[0]
fhigh = args.psdfreqargs[1]
delta_f = args.psdfreqargs[2]
flen     = int(fhigh / delta_f) + 1

 
# Mass model
if args.massmodel == "BBH_PowLawPeak":
    # BBH Power Law + Peak, GWTC-3 population, Sec 4.2 https://arxiv.org/pdf/2111.03604.pdf - Page 13
    massmodel = "SinglePeakSmoothedMassDistribution"
    mm_params = {"lam": 0.10, "alpha": 2.63, "beta": 1.26, "mpp": 33.07, "sigpp": 5.69, "mmax": 86.22, "mmin": 4.59, "delta_m": 4.82}

elif args.massmodel == "BNS_Gaussian":
    # BNS Gaussian model, see Farrow et al. 2019
    massmodel = "BNS_Gaussian"
    mm_params = {"mu": 1.33, "sigma": 0.09}

elif args.massmodel == "BNS_Uniform":
    # BNS Uniform
    massmodel = "BNS_Uniform"
    mm_params = {"um_min": 1., "um_max": 3., "mr_min" : 0.03, "mr_max" : 1. }

elif args.massmodel == "NSBH_LogUniform":
    # NSBH LogUniform model
    massmodel = "NSBH_LogUniform"
    mm_params = {"m1_min": 5., "m1_max": 100., "m2_min" : 1., "m2_max" : 3., "mr_min" : 0.03, "mr_max" : 1. }

# Local merger rate
R0 = args.localrate 

# Merger Rate Density model
if args.ratemodel == "MadauDickinson":
    # Eq 15 https://www.annualreviews.org/doi/10.1146/annurev-astro-081811-125615,  Madau & Dickinson (2014)
    rzmodel = "MadauDickinson"
    rzparams = {"R0": R0, "aa":2.7 , "bb":5.6, "zp":1.9}
elif args.ratemodel == "RzO3":
    # O3b catalog  - Eq 4 in https://arxiv.org/pdf/2111.03604.pdf
    rzmodel = "RofzO3"
    rzparams = {"R0": R0, "gamma":4.59 , "kappa":2.86, "z_peak":2.47}
elif args.ratemodel == "Oguri": 
    # Table 1, Eq 13 of https://arxiv.org/abs/1807.02584,  Oguri 2018
    rzmodel = "Oguri"
    rzparams = {"R0": R0, "a1":6.6e3, "a2":1.6, "a3":2.1, "a4":30}

# Number of processors
nproc = args.condornproc

# Global seed which sets all the other seeds
gseed = args.globalseed
print("Using a global seed",gseed)


# SNR distributions to be generated for different conditions on the source SNR given a SNR threshold
if args.snrargs :
    sn = args.snrargs
    # Flag for SNRthresh to be ON when true 
    SNRflag = sn[0]
    SNRthresh = sn[1]
    # NSamples*50 are generated for each redshift bin and there are 50 redshift bins from sn_zsmin to sn_zsmax, see constants.py
    # Default NSamples is 5000, best not to change this.
    NSamples = sn[2] 
 
if args.lensargs :
    ll = args.lensargs
    # zl - lens redshift, zs - source redshift, vd - velocity dispersion (km/s), ax - axis ratio (=min axis/maj axis) of lens galaxy, M - halo mass of lensing group/cluster (Msun)
    zlmin  = ll[0]; zlmax = ll[1]; zsmin =  ll[2]; zsmax =  ll[3]; vdmin =  ll[4]; vdmax =  ll[5]; axmin = ll[6]; axmax =  ll[7]; Mmin =  ll[8]; Mmax =   ll[9] 
else:
    zlmin = 0.001; zlmax = 3.; zsmin = 0.001; zsmax = 10.; vdmin = 10.; vdmax = 350.; axmin = 0.01; axmax = 1.0; Mmin = 14.; Mmax = 16.  

if args.concargs :
    cc = args.concargs
    # log (scatter) on sigma of the concentration, cMdef - halo mass definition e.g. 200m - 200 times the mean density of the Universe, Model for the concentration-mass of the lensing group/cluster, initialising cosmology - Planck15 
    concpars =  {"csig_scatter": 10**np.float(cc[0]), "cMdef": cc[1], "cmodel": cc[2], "colossus_cosm": cc[3]  }  
else:
    concpars = {"csig_scatter": 10**0.1, "cMdef": "200m", "cmodel": "diemer19", "colossus_cosm": "planck15"  }
 
