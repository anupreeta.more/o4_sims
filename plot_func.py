import numpy as np
from scipy.special import gamma
import pylab as plt
import lensprop as lp
from constants import *
from main import *
import srcprop as sp

pars = {"sensitivity": sensitivity, "aum_cosm": aum_cosm, "zsmin": zsmin, "zsmax": zsmax, "R0": R0, "rzmodel": rzmodel, "rzparams": rzparams, "detlabel" : detlabel, "srctype" : srctype,  "massmodel": massmodel, "mm_params": mm_params }

gpars = {"zlmin": zlmin, "zlmax":zlmax,  "vdmin":vdmin, "vdmax":vdmax, "axmin":axmin, "axmax":axmax, "aum_cosm": aum_cosm}

cpars = {"zlmin": zlmin, "zlmax":zlmax,  "Mmin":Mmin, "Mmax":Mmax, "aum_cosm":aum_cosm, "concpars": concpars}

mm = sp.srcMassModel(massmodel)
sm = sp.srcRedshiftModel(pars)
rm = sp.srcRateModel(rzmodel, R0)
gp = lp.galaxyProperties(gpars)
cp = lp.clusterProperties(cpars) 


def plot_halomassdist():
    zl = 0.5
    M200 = np.logspace(14.,16.,1000)
    mf = M200*0.
    ## calculate the halo mass function
    for ii in range(M200.size):
        mf[ii] = cp.get_halomassfunc(M200[ii],zl)
           
    Ntot = 90 #000
    halo_mass = np.zeros(Ntot)
    ## plot the distribution of halo masses drawn randomly such that they follow the halo mass function
    hmf=cp.getSpline_halo_zmass()
    for ii in range(Ntot):
        rand = np.random.uniform(0,1)
        halo_mass[ii] = hmf(zl,rand) 
        
    
    plt.plot(M200,mf*M200*5e8,label="Halo mass func")
    ## mf*M200 above because histogram binning, below, is in logspace  
    plt.hist(10**halo_mass,bins = np.logspace(14,16,50),label="Randomly drawn halo masses")
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()
    plt.savefig("/home/anupreeta.more/public_html/check_halomass_dist.pdf")
    plt.clf()


def plot_halo_zmass():
    mh_spl = cp.getSpline_halo_zmass()
    zl1 = 0.23
    zl2 = 0.35
    zl3 = 0.39
    Ntot = 1000
    rand_mf = np.random.uniform(0.,1.,Ntot)
    ####################### NEED TO FIX THIS
   #print(mh_spl(zl1,rand_mf))
    plt.hist(mh_spl(zl1,rand_mf),bins=np.linspace(14,16,100),alpha=0.8,label="1")
    plt.hist(mh_spl(zl2,rand_mf),bins=np.linspace(14,16,100),alpha=0.8,label="2")
    plt.hist(mh_spl(zl3,rand_mf),bins=np.linspace(14,16,100),alpha=0.5)
    #plt.xlim(14.,16.)
    plt.legend()
    plt.yscale("log")
    plt.savefig("/home/anupreeta.more/public_html/check_halomass_2dspl.pdf")
    plt.clf()

def plot_conc():
    M200m=np.logspace(12,16,1000)
    zl=np.zeros(1000)
   #conc=np.zeros(M200m.size)
    conc=cp.get_conc(np.log10(M200m),zl,concpars["csig_scatter"],concpars["cMdef"],concpars["cmodel"],concpars["colossus_cosm"])   
    plt.plot(M200m, conc)
    plt.yscale("log")
    plt.xscale("log")
    plt.savefig("/home/anupreeta.more/public_html/check_conc.pdf")
    plt.clf()

def plot_zlgal():
    ##### for gal-scale, zlens distrib
    Ntot = 1000
    rand_zl = np.random.uniform(0,1.,Ntot)
    zspl = gp.get_zl()
    plt.hist(zspl(rand_zl), histtype="step",label="zlens distrib")
    plt.xlabel("zlens")
    plt.legend()
    plt.savefig("/home/anupreeta.more/public_html/check_zlprob.pdf")
    plt.clf()


def plot_vdgal():
    ##### for gal-scale, vel. disp. distrib
    Ntot = 5000
    rand_vd = np.random.uniform(0,1.,Ntot)
    vdspl = gp.get_vd()
   #plt.scatter(vdspl(rand_vd),rand_vd,label="vel. disp distrib")
    plt.hist(vdspl(rand_vd), histtype="step", bins=np.logspace(1,np.log10(350),20),label="vel. disp distrib")
    plt.xlabel("vdisp (km/s)")
    plt.legend()
    plt.savefig("/home/anupreeta.more/public_html/check_vdprob.pdf")
    plt.clf()


def plot_pofaxratio():
 # ax = plt.subplot(2,2,1)
   pofqspl = gp.get_axratio()
   Ntot = 1000
   pofq = np.random.uniform(0,1,Ntot)    
   plt.hist(pofqspl(pofq), histtype="step",label="Points drawn from P(q)")
   plt.xlabel("Axis ratio (q)")
   plt.legend()
   plt.savefig("/home/anupreeta.more/public_html/check_probofqq.pdf")
   plt.clf()

### Randomly drawn numbers from uniform distribution that follows the z_bbh distribution
def plot_pofzsrc():
    Ntot = 10000
    zz_spl = sm.get_zsrc()
    probz = np.random.uniform(0,1,Ntot)
    plt.hist(zz_spl(probz), histtype="step")
    plt.savefig("/home/anupreeta.more/public_html/check_zsprob.pdf")
    plt.clf()

### Check rates distribution
def plot_rofz():
    zs = np.linspace(0.001,10.,1000)
    zsfunc = zs*0.
    for ii in range(zs.size):
        zsfunc[ii] = rm.get_Rofz(zs[ii], rzparams)
    plt.plot(zs,zsfunc)
    plt.yscale("log")
    plt.savefig("/home/anupreeta.more/public_html/check_rofzR0.pdf")
    plt.clf()

def plot_srcmass():
    Nsamples = 10000
    m1,m2 = mm.getMasses(mm_params, Nsamples)
    plt.hist(m1, bins=20, histtype = "step")
    plt.hist(m2, bins=20, histtype = "step")
    plt.legend()
    plt.savefig("/home/anupreeta.more/public_html/check_srcmass.pdf")
    plt.clf()

def plot_pofsnr():
    Ntot = 15000
    print(sensitivity)
    #zsrc=0.7857
  # zz_spl = sm.get_zsrc()
  #
  # zsprob = np.random.uniform(0,1.,Ntot)
  # zsrc = zz_spl(zsprob)
    zs = np.array([0.11,0.53, 1.15, 3.2])
    def calc_snr(zs):
        zsrc = np.ones(Ntot)*zs
        snprob = np.random.uniform(0,1.,Ntot)
        uSNR = zsrc*0.
        zsarr, splinearr, z_snrmax_spl =sm.init_snrspline()
        ## finds index of the number which is just less than zsrc
        sidx = np.searchsorted(zsarr, zsrc)
        snprob = np.random.uniform(0,1.,Ntot)
        for ii in range(Ntot):
            if 1:
                fac1 = splinearr["%.3f" % (zsarr[sidx[ii]])](snprob[ii])
                fac2 = z_snrmax_spl(zsrc[ii])
                fac1 = min(max(0, fac1), 1)
                uSNR[ii] = fac1 * fac2
            else:
                uSNR[ii] = 0
        return uSNR
    for kk in range(4):
        ax=plt.subplot(2,2,kk+1)
        uSNR=calc_snr(zs[kk])
        plt.hist(np.log10(uSNR),bins=20,histtype="step", label=zs[kk])
        plt.legend()
        plt.xlim(-2,3)
        plt.yscale("log")
    plt.savefig("/home/anupreeta.more/public_html/checkpsnr_%s.pdf"%(sensitivity[0]))
    plt.clf()
 

#plot_halomassdist()
#plot_halo_zmass()
#
#
plot_srcmass()
#plot_pofsnr()

## Checks done
#plot_vdgal()
#plot_pofaxratio()
#plot_conc()
#plot_pofzsrc()
#plot_rofz()
#plot_zlgal()
