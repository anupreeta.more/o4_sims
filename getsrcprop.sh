#!/bin/bash

RANDOM=42; 

##DIFF=$((2000-1+1))
##echo $(($(($RANDOM%$DIFF))+1)) 

 awk -v var=$1 -f get_src_rownum.awk $2 > $3
 rowtot=`wc -l $3 | awk '{print $1}'`
 DIFF=$(($rowtot-1+1))
 rand=$(($(($RANDOM%$DIFF))+1)) 
 row=`awk -v var=$rand '{if(NR==var) print $NF}' "$3"`
 imnum=`awk -v var=$rand '{if(NR==var) print $1}' "$3"`
#row=`awk -v var=$4 '{if(NR==var) print $NF}' "$3"`
#imnum=`awk -v var=$4 '{if(NR==var) print $1}' "$3"`
 echo "## rowtot, rand, rownum, imnum" $rowtot $rand $row $imnum
 awk  -v row=$row -v imnum=$imnum  -f get_improp.awk $2  > $4 
