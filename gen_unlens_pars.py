#!/usr/bin/env python
import numpy as np
import snr as snr
from main import *
import srcprop as sp

# nproc = nth processor on which the code will run,  sn_zbins = total redshift bins
print("Using a global seed",gseed)
np.random.seed(gseed+400+np.int(nproc))

print("Number of samples per processor are", NSamples)
if NSamples < 1000 :
    print("===========================WARNING======================== \n NSamples may NOT be sufficiently large and will result in sparser SNR distribution \n ======================================" )


# Generate and save network SNRs  as a function of source redshift from sn_zsmin to sn_zsmax ( limits are set in constants.py )
print("Generating SNR distribution in the redshift range:", sn_zsmin, sn_zsmax)
print("Running processor number:",nproc, "on a total of ", sn_zbins, " redshift bins")

# Initialise Mass Model
print("MassModel : ", massmodel)
mm=sp.srcMassModel(massmodel)
srcmasses=mm.getMasses(mm_params,NSamples)

# Initialise detector frame source params
dfp=sp.detFrameParams()
obsparams=dfp.getParams(NSamples)


pars = {"sensitivity": sensitivity, "aum_cosm": aum_cosm, "zsmin": zsmin, "zsmax": zsmax, "R0": R0, "rzmodel": rzmodel, "rzparams": rzparams, "detlabel": detlabel, "srctype": srctype}
print("Using", pars)
print("Note: no other --lensargs are used apart from source redshifts")

# Draw source redshifts as per the redshift distribution given in rates model and local rate R0 
sm = sp.srcRedshiftModel(pars)
zsspl = sm.get_zsrc()
zsprob=np.random.uniform(0,1.,NSamples)
zsrc=zsspl(zsprob)

# Generate the unlensed SNR distribution based on SNRflag and SNRthresh and store them in output files, see snr.py for details
sn=snr.SNR(sensitivity=[sensitivity], detectors=detectors, flow=flow, fhigh=fhigh, delta_f=delta_f, is_asd_file=True)
sn.getsnr_unlens(zsrc, nproc, detlabel, srctype, srcmasses, obsparams, SNRflag, SNRthresh)

